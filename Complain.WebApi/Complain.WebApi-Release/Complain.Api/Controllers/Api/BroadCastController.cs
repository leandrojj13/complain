using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Complain.BL.Abstract;
using Complain.BL.Dto;
using Complain.Core.Models;
using Complain.DataModel.Entities;
using Complain.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Complain.Api.Controllers.Api
{
    // [Authorize]
    public class BroadCastController : BaseApiController<BroadCast, BroadCastDto>
    {
        private readonly IBroadCastRepository _broadCastRepository;
        private readonly IBroadCastService _broadCastService;

        public BroadCastController(IBroadCastRepository broadCastRepository,
                                    IBroadCastService broadCastService, 
                                    IMapper mapper)
            : base(broadCastRepository, mapper)
        {
            _broadCastRepository = broadCastRepository;
            _broadCastService = broadCastService;
        }

        // POST api/values/
        [HttpPost]
        public override async Task<IActionResult> Create([FromBody] BroadCastDto dto)
        {
            if (dto == null)
            {
                return BadRequest(new Result(){StatusCode = HttpStatusCode.BadRequest});
            }
            var model = _mapper.Map<BroadCast>(dto);

            var isSuccess = await _broadCastService.SendBroadCastAsync(model);

            if(!isSuccess)
                return BadRequest(new Result() { Message = "Error al enviar notificación", StatusCode = HttpStatusCode.BadRequest});
            
            var resultRepository = await _db.Add(model);
            if (!resultRepository.Success)
                return BadRequest(resultRepository);

            await _db.Save();

            var result = new Result<BroadCastDto>()
            {
                Entity = _mapper.Map<BroadCastDto>(model),
                Success = true,
                StatusCode = HttpStatusCode.Created
            };
            return CreatedAtAction("Get", new { id = model.Id }, result);
        }
    }
}
