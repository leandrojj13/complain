using System;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Complain.BL.Abstract;
using Complain.Core;
using Complain.Core.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Authorization;
using AspNet.Security.OAuth.Validation;

namespace Complain.Api.Controllers
{
    //[Authorize(ActiveAuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public abstract class  BaseApiController<T, TD> : Controller where T : class, IEntityBase, new()
                                                                 where TD : class, IEntityBaseDto, new()
    {
        protected readonly IEntityBaseRepository<T> _db;
        protected readonly IMapper _mapper;

        protected BaseApiController(IEntityBaseRepository<T> db, IMapper mapper)
        {
             _db = db;
             _mapper = mapper;
        }

        // GET: api/values
        [HttpGet]
        public virtual async Task<IActionResult> GetAll()
        {
            var list = await _db.GetAll();
            var listDto = _mapper.Map<List<TD>>(list);
            var result = new GetManyResult<TD>()
            {
                Entities = listDto, 
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(result);
        }

        // GET api/values/5 
        [HttpGet("{id}")]
        public virtual async Task<IActionResult> Get(Guid id)
        {
            var model = await _db.Find(id);

            if (model == null)
            {
                return NotFound(new Result(){StatusCode = HttpStatusCode.NotFound});
            }

            var dto = _mapper.Map<TD>(model);
            var result = new Result<TD>()
            {
                Entity = dto, 
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(result);
        }
 
        // POST api/values/
        [HttpPost]
        public virtual async Task<IActionResult> Create([FromBody] TD dto)
        {
            if (dto == null)
            {
                return BadRequest(new Result(){StatusCode = HttpStatusCode.BadRequest});
            }
            var model = _mapper.Map<T>(dto);

            var resultRepository = await _db.Add(model);
            if (!resultRepository.Success)
                return BadRequest(resultRepository);

            await _db.Save();

            var result = new Result<TD>()
            {
                Entity = _mapper.Map<TD>(model),
                Success = true,
                StatusCode = HttpStatusCode.Created
            };
            return CreatedAtAction("Get", new { id = model.Id }, result);
        }

        [HttpPut("{id}")]
        public virtual async Task<IActionResult> Update(string id, [FromBody] TD dto)
        {
            Guid guidID = Guid.Empty;
            if (dto == null || !Guid.TryParse(id, out guidID))
            {
                return BadRequest(new Result(){StatusCode = HttpStatusCode.BadRequest});
            }

            var find = await _db.Find(Guid.Parse(id));
            if (find == null)
            {
                return NotFound(new Result(){StatusCode = HttpStatusCode.NotFound});
            }
            var model = _mapper.Map<T>(dto);
            var result = _db.Update(model);
            if (result.Success){
               await _db.Save();
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpDelete("{id}")]
        public virtual async Task<IActionResult> Delete(string id)
        {
            Guid guidID = Guid.Empty;
            if (!Guid.TryParse(id, out guidID))
            {
                return BadRequest(new Result(){StatusCode = HttpStatusCode.BadRequest});
            }

            var model = await _db.Find(Guid.Parse(id));
            if (model == null)
            {
                return NotFound(new Result(){StatusCode = HttpStatusCode.NotFound});
            }
            //
            var result = _db.Remove(model);
            if (result.Success)
            {
               await _db.Save();
               return Ok(result);
            }

            return BadRequest(result);
        }

    }
}
