﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Complain.DataModel.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Complain.BL.Abstract;
using Complain.BL.Repositories;
using AutoMapper;
using Complain.BL.Mappers;
using FluentValidation.AspNetCore;
using Complain.BL.Validators;
using Complain.Services.Interfaces;
using Complain.Services.Implementation;
using Complain.Core.Models;
using Complain.DataModel.Entities;
using AspNet.Security.OpenIdConnect.Primitives;
using Microsoft.AspNetCore.Identity;

namespace Complain.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Adds services required for using options.
            services.AddOptions();

            // Register the IConfiguration instance which paths binds against.
            services.Configure<BroadCastConfig>(Configuration.GetSection("BroadCastConfig"));
            services.Configure<PathsConfig>(Configuration.GetSection("PathsConfig"));
            services.Configure<TownHall>(Configuration.GetSection("TownHall"));

            //services.AddDbContext<ComplainDbContext>(opt => opt.UseInMemoryDatabase());

            // Add entity framework services.
            services.AddDbContext<ComplainDbContext>(options =>
            {
                // Configure the context to use Microsoft SQL Server.
                options.UseSqlServer( Configuration.GetConnectionString("DefaultConnection"),  b => b.MigrationsAssembly("Complain.Api"));

                // Register the entity sets needed by OpenIddict.
                // Note: use the generic overload if you need
                // to replace the default OpenIddict entities.
                options.UseOpenIddict();
            }); 

            // Register the Identity services.
            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<ComplainDbContext, Guid>()
                .AddDefaultTokenProviders()
                .AddUserStore<UserStore<ApplicationUser, ApplicationRole, ComplainDbContext, Guid>> ()
                .AddRoleStore<RoleStore<ApplicationRole, ComplainDbContext, Guid>>();

            // Configure Identity to use the same JWT claims as OpenIddict instead
            // of the legacy WS-Federation claims it uses by default (ClaimTypes),
            // which saves you from doing the mapping in your authorization controller.
            services.Configure<IdentityOptions>(options =>
            {
                options.ClaimsIdentity.UserNameClaimType = OpenIdConnectConstants.Claims.Name;
                options.ClaimsIdentity.UserIdClaimType = OpenIdConnectConstants.Claims.Subject;
                options.ClaimsIdentity.RoleClaimType = OpenIdConnectConstants.Claims.Role;

                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 5;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
            });

            // Register the OpenIddict services.
            services.AddOpenIddict(options =>
            {
                // Register the Entity Framework stores.
                options.AddEntityFrameworkCoreStores<ComplainDbContext>();

                // Register the ASP.NET Core MVC binder used by OpenIddict.
                // Note: if you don't call this method, you won't be able to
                // bind OpenIdConnectRequest or OpenIdConnectResponse parameters.
                options.AddMvcBinders();
                
                // Enable the token endpoint.
                options.EnableTokenEndpoint("/connect/token");

                // Enable the password flow.
                options.AllowPasswordFlow();

                // During development, you can disable the HTTPS requirement.
                options.DisableHttpsRequirement();
                

                // Note: to use JWT access tokens instead of the default
                // encrypted format, the following lines are required:
                //
                // options.UseJsonWebTokens();
                // options.AddEphemeralSigningKey();
            });

            // Add mappers services.
            services.AddAutoMapper(cfg => {
                cfg.AddProfile<ComplainMappingProfile>();
            });

            // Add CORS services.
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAdminOrigin",
                builder => builder.WithOrigins(Configuration.GetValue<string>("AdminUrl", null))
                                    .AllowAnyOrigin()
                                    .AllowAnyHeader()
                                    .AllowAnyMethod());
            });

            // Add framework services.
            services.AddMvc()
                    .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<ComplaintTypeValidator>());

            services.AddSingleton(Mapper.Configuration);

            services.AddScoped<IMapper>(sp => 
            new Mapper(sp.GetRequiredService<AutoMapper.IConfigurationProvider>(), sp.GetService));
            //Repositories 
            services.AddScoped<IComplaintTypeRepository, ComplaintTypeRepository>();
            services.AddScoped<IComplaintRepository, ComplaintRepository>();
            services.AddScoped<IIncrementRepository, IncrementRepository>();
            services.AddScoped<IBroadCastRepository, BroadCastRepository>();
            //Servises 
            services.AddTransient<IComplaintService, ComplaintService>();
            services.AddTransient<IComplaintTypeService, ComplaintTypeService>();
            services.AddTransient<IComplaintsInquiriesService, ComplaintsInquiriesService>();
            services.AddTransient<IBroadCastService, BroadCastService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, ComplainDbContext DbContext, UserManager<ApplicationUser> _userManager)
        {
            app.UseDeveloperExceptionPage();

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseIdentity();
            // Add a middleware used to validate access
            // tokens and protect the API endpoints.
            app.UseOAuthValidation();

            app.UseOpenIddict();

            //if (env.IsDevelopment())
            //{
                DbInitializer.Initialize(DbContext, _userManager, Configuration);
            //}
            
            app.UseStaticFiles();
            
            app.UseCors("AllowAdminOrigin");

            app.UseMvc();

            app.UseWelcomePage();

        }
    }
}
