using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Complain.Core;
using Complain.DataModel.Entities;
using Complain.DataModel.Entities.Enums;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Complain.DataModel.SampleData{
    public class IncrementSample  
    {
        private static List<Increment> _increments;

        static IncrementSample()
        {
            if (_increments == null)
            {
                Increments = new List<Increment>()
                {
                    new Increment() { IncrementType = IncrementType.Complaint }
                };
            }
        }

        public static List<Increment> Increments { get => _increments; set => _increments = value; }

    }
}