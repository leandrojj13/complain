using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Complain.Core;
using Complain.DataModel.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Complain.DataModel.SampleData{
    public class ComplaintTypeSample  
    {
        private static List<ComplaintType> _complaintsTypes;

        static ComplaintTypeSample()
        {
            if (_complaintsTypes == null)
            {
                ComplaintsTypes = new List<ComplaintType>()
                {
                    new ComplaintType{ Id=Guid.NewGuid(), Codigo= "VRTDR", Name="Vertedero",Description="Lugar donde se almacena una gran cantidad basura", CreatedDate = new DateTime(), CreatedBy = Guid.Empty },
                    new ComplaintType{ Id=Guid.NewGuid(), Codigo= "NOPELC", Name="No Pasa el Camión", CreatedDate = new DateTime(), CreatedBy = Guid.Empty },
                    new ComplaintType{ Id=Guid.NewGuid(), Codigo= "PRQDCI", Name="Parque Descuidado", CreatedDate = new DateTime(), CreatedBy = Guid.Empty },
                    new ComplaintType{ Id=Guid.NewGuid(), Codigo= "CLLNALT", Name="Calles no Asfaltadas", CreatedDate = new DateTime(), CreatedBy = Guid.Empty },
                    new ComplaintType{ Id=Guid.NewGuid(), Codigo= "ESCBRO", Name="Escombros", CreatedDate = new DateTime(), CreatedBy = Guid.Empty },
                    new ComplaintType{ Id=Guid.NewGuid(), Codigo= "ALBDPQE", Name="Alumbrado de Parque", CreatedDate = new DateTime(), CreatedBy = Guid.Empty },
                    new ComplaintType{ Id=Guid.NewGuid(), Codigo= "DRNJTPAD", Name="Drenaje Tapado", CreatedDate = new DateTime(), CreatedBy = Guid.Empty },
                    new ComplaintType{ Id=Guid.NewGuid(), Codigo= "SMFRDD", Name="Semáforo Dañado", CreatedDate = new DateTime(), CreatedBy = Guid.Empty },
                    new ComplaintType{ Id=Guid.NewGuid(), Codigo= "ABLCID", Name="Arboles Caídos", CreatedDate = new DateTime(), CreatedBy = Guid.Empty },
                    new ComplaintType{ Id=Guid.NewGuid(), Codigo= "OTROS", Name="Otros", CreatedDate = new DateTime(), CreatedBy = Guid.Empty },
                };
            }
        }

        public static List<ComplaintType> ComplaintsTypes { get => _complaintsTypes; set => _complaintsTypes = value; }

    }
}