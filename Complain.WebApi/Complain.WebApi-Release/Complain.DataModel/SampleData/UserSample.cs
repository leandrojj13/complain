using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Complain.Core;
using Complain.DataModel.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Complain.DataModel.SampleData{
    public class UserSample  
    {
        private static List<ApplicationUser> _users;

        static UserSample()
        {
            if (_users == null)
            {
                Users = new List<ApplicationUser>()
                {
                    new ApplicationUser { Id = Guid.NewGuid(), UserName = "admin@complain.com", Email ="admin@complain.com", GivenName = "Administrador" },
                    new ApplicationUser { Id = Guid.NewGuid(), UserName = "leandro.jimenez@vmeta.com.do", Email ="leandro.jimenez@vmeta.com.do", GivenName = "Leandro Jimenez" },
                    new ApplicationUser { Id = Guid.NewGuid(), UserName = "jose.aquino@vmeta.com.do", Email ="jose.aquino@vmeta.com.do", GivenName = "José Aquino" }
                };
            }
        }

        public static List<ApplicationUser> Users { get => _users; set => _users = value; }

    }
}