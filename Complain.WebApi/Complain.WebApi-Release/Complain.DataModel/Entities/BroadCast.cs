using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Complain.Core;
using Complain.DataModel.Entities.Enums;

namespace Complain.DataModel.Entities{
    public class BroadCast : IAuditEntityBase
    {
        [Column("BroadCastId")]
        public Guid Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Message { get; set; }

        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public bool Disabled { get; set; }
        public bool Deleted { get; set; }
    }
}