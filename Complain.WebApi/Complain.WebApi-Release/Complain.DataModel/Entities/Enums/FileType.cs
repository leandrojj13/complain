namespace Complain.DataModel.Entities.Enums{
    public enum FileType: byte
    {
        Image,
        Video,
        Audio
    }
}