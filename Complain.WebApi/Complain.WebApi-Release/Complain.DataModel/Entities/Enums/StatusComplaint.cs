namespace Complain.DataModel.Entities.Enums{
    public enum StatusComplaint: byte
    {
        Send,
        Saved,
        InProcess,
        Attended
    }
}