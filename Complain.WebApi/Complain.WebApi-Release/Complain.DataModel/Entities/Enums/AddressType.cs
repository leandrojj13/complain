namespace Complain.DataModel.Entities.Enums{
    public enum AddressType: byte
    {
        GPS,
        Location
    }
}