using System;
using System.ComponentModel.DataAnnotations.Schema;
using Complain.Core;
using Complain.DataModel.Entities.Enums;

namespace Complain.DataModel.Entities{
    public class Attached : IAuditEntityBase
    {
        [Column("AttachedId")]
        public Guid Id { get; set; }
        public FileType FileType { get; set; }
        public string FilePath { get; set; }
        public string Name { get; set; }

        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public bool Disabled { get; set; }
        public bool Deleted { get; set; }

        public Guid ComplaintId { get; set; }
        public virtual Complaint Complaint { get; set; }
    }
}