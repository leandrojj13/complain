using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Complain.Core;
using Complain.DataModel.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Complain.DataModel.Context
{
    public class ComplainDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>{

        public ComplainDbContext(DbContextOptions<ComplainDbContext> options) : base(options){

        }

        public virtual DbSet<Attached> Attachments { get; set; }
        public virtual DbSet<Complaint> Complaints { get; set; }
        public virtual DbSet<Increment> Increments { get; set; }
        public virtual DbSet<BroadCast> BroadCasts { get; set; }
        public virtual DbSet<ComplaintType> ComplaintsTypes { get; set; }

        // public async new Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        // {
        //     CreateAuditLogInformation();
        //     ModifyAuditInformation();
        //     return await base.SaveChangesAsync(cancellationToken);
        // }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ApplicationUser>(b =>
            {
                b.Property(u => u.Id).HasDefaultValueSql("newsequentialid()");
            });

            builder.Entity<ApplicationRole>(b =>
            {
                b.Property(u => u.Id).HasDefaultValueSql("newsequentialid()");
            });
        }
        
        // public void CreateAuditLogInformation()
        // {
        //     var entitiesAdded = ChangeTracker.Entries<IAuditEntityBase>()
        //                         .Where(x => x.State == EntityState.Added);
        //     foreach (var entry in entitiesAdded)
        //     {
        //         entry.Entity.CreatedBy = Guid.Empty;
        //         entry.Entity.CreatedDate = DateTime.Now;
        //     }
        // }

        // public void ModifyAuditInformation()
        // {
        //     var entitiesModified = ChangeTracker.Entries<IAuditEntityBase>()
        //                             .Where(x => x.State == EntityState.Modified);
        //     foreach (var entry in entitiesModified)
        //     {
        //         entry.Entity.ModifiedBy = Guid.Empty;
        //         entry.Entity.ModifiedDate = DateTime.Now;
        //     }
        // }

    }
}