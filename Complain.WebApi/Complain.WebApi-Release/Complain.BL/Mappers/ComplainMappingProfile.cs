using System;
using AutoMapper;
using Complain.BL.Dto;
using Complain.DataModel.Entities;

namespace Complain.BL.Mappers{
    public class ComplainMappingProfile : Profile{

        public ComplainMappingProfile(){

            CreateMap<string, Guid>().ConvertUsing<GuidStringConverter>();
            CreateMap<Guid, string>().ConvertUsing<GuidStringConverter>();
            //
            CreateMap<Guid?, string>().ConvertUsing<GuidStringConverter>();
            CreateMap<string, Guid?>().ConvertUsing<GuidStringConverter>();

            CreateMap<RegisterDto, ApplicationUser>()
            .ForMember(ApplicationUser=> ApplicationUser.UserName, cfg => 
                cfg.MapFrom(resgiterDto => resgiterDto.Email))
            .ForMember(ApplicationUser=> ApplicationUser.GivenName, cfg => 
                cfg.MapFrom(resgiterDto => resgiterDto.Name));
                
            CreateMap<ApplicationUser, UserDto>();

            CreateMap<Complaint, ComplaintDto>()
            .ForMember(complaintDto=> complaintDto.ComplaintTypeName, 
                cfg => cfg.MapFrom(complaint => complaint.ComplaintType.Name))
            .ForMember(complaintDto=> complaintDto.ComplaintTypeImageUrl, 
                cfg => cfg.MapFrom(complaint => complaint.ComplaintType.ImageUrl))
            .ReverseMap();

            CreateMap<ComplaintType, ComplaintTypeDto>()
            .ReverseMap();

            CreateMap<Attached, AttachedDto>()
            .ForMember(attachedDto=> attachedDto.Url, 
                cfg => cfg.MapFrom(attached => attached.FilePath))
            .ReverseMap();

            CreateMap<BroadCast, BroadCastDto>()
            .ReverseMap();
        }   
    }
}
