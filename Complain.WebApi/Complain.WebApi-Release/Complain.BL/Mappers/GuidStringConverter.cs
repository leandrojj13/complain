using System;
using AutoMapper;

namespace Complain.BL.Mappers{
    
    public class GuidStringConverter : ITypeConverter<string, Guid>, ITypeConverter<Guid, string>,
        ITypeConverter<string, Guid?>, ITypeConverter<Guid?, string>
    {
        public Guid Convert(string source, Guid destination, ResolutionContext context)
        {
            if (string.IsNullOrEmpty(source))
                return Guid.Empty;

            Guid.TryParse(source, out Guid x);
            return x;
        }

        public string Convert(Guid source, string destination, ResolutionContext context)
        {
            return source.ToString("D");
        }

        public Guid? Convert(string source, Guid? destination, ResolutionContext context)
        {
            if (string.IsNullOrEmpty(source))
                return null;

            if (Guid.TryParse(source, out Guid result))
                return result;

            return null;
        }

        public string Convert(Guid? source, string destination, ResolutionContext context)
        {
            if (source.HasValue)
                return source.Value.ToString("D");

            return string.Empty;
        }
    }
}