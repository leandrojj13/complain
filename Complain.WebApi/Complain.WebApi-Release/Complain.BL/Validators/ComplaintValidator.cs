using Complain.DataModel.Entities;
using FluentValidation;

namespace Complain.BL.Validators{
    public class ComplaintValidator :  AbstractValidator<Complaint>{
        
        public ComplaintValidator(){

            RuleFor(complaint => complaint.Description).NotNull()
                                                        .NotEmpty();
        }
    }
}