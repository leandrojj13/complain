using Complain.DataModel.Entities;
using FluentValidation;

namespace Complain.BL.Validators{
    public class ComplaintTypeValidator :  AbstractValidator<ComplaintType>{
        
        public ComplaintTypeValidator(){
            RuleFor(complaintType => complaintType.Codigo).NotNull()
                                                        .NotEmpty();
            RuleFor(complaintType => complaintType.Name).NotNull()
                                                        .NotEmpty()
                                                        .Length(5, 20);
            RuleFor(complaintType => complaintType.Description).MaximumLength(50);
        }
    }
}