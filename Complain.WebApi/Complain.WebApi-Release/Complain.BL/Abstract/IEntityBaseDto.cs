namespace Complain.BL.Abstract
{
    public interface IEntityBaseDto
    {
        string Id { get; set; }
    }
}
