using System;
using System.Collections.Generic;
using Complain.BL.Abstract;
using Complain.DataModel.Entities.Enums;

namespace Complain.BL.Dto
{
    public class BroadCastDto : IEntityBaseDto{
        public string Id { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
    }
}