using Complain.BL.Abstract;

namespace Complain.BL.Dto
{
    public class ComplaintTypeDto : IEntityBaseDto{

        public string Id { get; set; }    
        public string Name { get; set; }
        public string Codigo { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
    }
}