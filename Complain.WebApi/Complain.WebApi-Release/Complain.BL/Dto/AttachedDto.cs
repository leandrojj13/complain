using System;
using System.Collections.Generic;
using Complain.BL.Abstract;
using Complain.DataModel.Entities.Enums;

namespace Complain.BL.Dto
{
    public class AttachedDto : IEntityBaseDto{
        public string Id { get; set; }
        public FileType FileType { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }
        public string Url { get; set; }
    }
}