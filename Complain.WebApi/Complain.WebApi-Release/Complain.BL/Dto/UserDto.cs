using System;
using Complain.BL.Abstract;

namespace Complain.BL.Dto
{
    public class UserDto : IEntityBaseDto{

        public string Id { get; set; }
        public string GivenName { get; set; }
        public string Email { get; set; }

    }
}