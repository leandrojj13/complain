using System;
using Complain.BL.Abstract;

namespace Complain.BL.Dto
{
    public class RegisterDto : IEntityBaseDto{

        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

    }
}