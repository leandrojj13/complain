using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Complain.BL.Abstract;
using Complain.DataModel.Entities;
using Complain.Services.Interfaces;

namespace Complain.Services.Implementation
{
    public class ComplaintsInquiriesService : IComplaintsInquiriesService
    {
        private readonly IComplaintTypeRepository _complaintTypeRepository;
        private readonly IComplaintRepository _complaintRepository;

        public ComplaintsInquiriesService(IComplaintTypeRepository complaintTypeRepository,
                                IComplaintRepository complaintRepository)
        {
            _complaintTypeRepository = complaintTypeRepository;
            _complaintRepository = complaintRepository;
        }

        public async Task<object> GetComplaintsMadeBarChart(DateTime? filterDate)
        {
            var complaintsTypes = await _complaintTypeRepository.GetAll(x => x.Complaints);
            var complaintsMade =
                complaintsTypes.Select(complaintType =>
                {
                    int ComplaintCount;
                    if(filterDate.HasValue)
                        ComplaintCount = complaintType.Complaints.Where(c => c.CreatedDate.Date == filterDate.GetValueOrDefault().Date).Count();
                    else
                        ComplaintCount = complaintType.Complaints.Count();
                        
                    return new
                    {
                        Id = complaintType.Id,
                        Name = complaintType.Name,
                        Count = ComplaintCount
                    };
                }).OrderByDescending(complaintType => complaintType.Name.Length);
            return complaintsMade;
        }

        public async Task<object> GetComplaintsMadeLineChartAnnual()
        {
            var complaints = await _complaintRepository.GetAll();
            var complaintsMade =
                complaints.GroupBy(z => z.CreatedDate.Year)
                          .Select(f =>
                          {
                              return new
                              {
                                  Count = f.Count(),
                                  Year = f.FirstOrDefault().CreatedDate.Year,
                              };
                          })
                          .OrderBy(x => x.Year);
            return complaintsMade;
        }

        public async Task<object> GetComplaintsMadeLineChartMonthlyByYear(int year)
        {
            var complaints = await _complaintRepository.GetAll();
            var complaintsMade =
                complaints.Where(c => c.CreatedDate.Year == year)
                          .GroupBy(z => z.CreatedDate.Month)
                          .Select(f =>
                          {
                              var MonthName = f.FirstOrDefault().CreatedDate.ToString("MMMM", new CultureInfo("es-DO"));
                              return new
                              {
                                  Count = f.Count(),
                                  MonthNumber = f.FirstOrDefault().CreatedDate.Month,
                                  MonthName = $"{char.ToUpper(MonthName[0])}{MonthName.Substring(1)}"
                              };
                          })
                          .OrderBy(x => x.MonthNumber);
            return complaintsMade;
        }
    }
}