using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Complain.Core.Models;
using Complain.DataModel.Entities;
using Complain.Services.Interfaces;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Complain.Services.Implementation
{
    public class BroadCastService : IBroadCastService
    {
       private readonly BroadCastConfig _broadCastConfig;

        public BroadCastService(IOptions<BroadCastConfig> broadCastConfig){
            _broadCastConfig = broadCastConfig.Value;
        }
        public async Task<bool> SendBroadCastAsync(BroadCast broadCast)
        {
            using (var client = new HttpClient())
            {
                try {
                    client.BaseAddress = new Uri(_broadCastConfig.ApiUrl);

                    var data = new
                    {
                        profile = "complaintsecurityprofile",
                        notification = new {
                                        title =  broadCast.Title,
                                        message = broadCast.Message
                        },
                        send_to_all = true
                    };

                    var content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8,
                                    "application/json");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _broadCastConfig.ApiToken);

                    var result = await client.PostAsync("push/notifications", content);

                    var message = result.EnsureSuccessStatusCode();

                    return message.IsSuccessStatusCode;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
    }
}