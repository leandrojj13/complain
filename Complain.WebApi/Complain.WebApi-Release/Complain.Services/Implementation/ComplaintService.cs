using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Complain.BL.Abstract;
using Complain.BL.Dto;
using Complain.Core.Models;
using Complain.DataModel.Entities;
using Complain.DataModel.Entities.Enums;
using Complain.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;

namespace Complain.Services.Implementation
{
    public class ComplaintService : IComplaintService
    {
        private readonly PathsConfig _pathsConfig;
        private readonly TownHall _townHall;
        private readonly IHostingEnvironment _appEnv;
        private readonly IIncrementRepository _incrementRepository;
        private readonly IBroadCastService _broadCastService;


        public ComplaintService(IOptions<PathsConfig> pathsConfig,  
                                IOptions<TownHall> townHall,
                                IHostingEnvironment appEnv,
                                IIncrementRepository incrementRepository,
                                IBroadCastService broadCastService){
            _pathsConfig = pathsConfig.Value;
            _townHall = townHall.Value;
            _appEnv = appEnv;
            _incrementRepository = incrementRepository;
            _broadCastService = broadCastService;
        }
        public async Task<IEnumerable<Attached>> SaveAttachmentsAsync(List<AttachedDto> attachmentsDto, string dirname)
        {
            var attachments = new List<Attached>();
            foreach (var attachedDto in attachmentsDto)
            {
                Attached attached;
                switch(attachedDto.FileType){
                    case FileType.Image: 
                        attached = await SaveImageAsync(attachedDto, dirname);
                        attachments.Add(attached);
                        break;
                    case FileType.Video:
                        attached = await SaveVideoAsync(attachedDto, dirname);
                        attachments.Add(attached);
                        break;
                    case FileType.Audio: 
                        attached = await SaveAudioAsync(attachedDto, dirname);
                        attachments.Add(attached);
                        break;
                }
            }
            return attachments;
        }

        public async Task<Attached> SaveAudioAsync(AttachedDto attached, string dirname)
        {
            try{
                var fisicalPath = $"{dirname}/audios/";
                var upload = Path.Combine(_appEnv.WebRootPath, _pathsConfig.Complaints, fisicalPath);
                Directory.CreateDirectory(upload);

                var bytes = StringToBytesArray(attached.Data);
                using (var imageFile = new FileStream(Path.Combine(upload, attached.Name), FileMode.Create))
                {
                    await imageFile.WriteAsync(bytes, 0, bytes.Length); 
                    imageFile.Flush();
                }
                return new Attached(){
                    Name = attached.Name,
                    FileType = FileType.Audio,
                    FilePath =  $"{_pathsConfig.Complaints}{fisicalPath}{attached.Name}"
                };
            }catch(Exception ex){
                var x = ex;
                return null;
            }
        }

        public async Task<Attached> SaveImageAsync(AttachedDto attached, string dirname)
        {     
            try{
                var fisicalPath = $"{dirname}/images/";
                var upload = Path.Combine(_appEnv.WebRootPath, _pathsConfig.Complaints, fisicalPath);
                Directory.CreateDirectory(upload);

                var bytes = Convert.FromBase64String(attached.Data);
                var imageName = $"{Guid.NewGuid()}.jpeg";
                using (var imageFile = new FileStream(Path.Combine(upload, imageName), FileMode.Create))
                {
                    await imageFile.WriteAsync(bytes, 0, bytes.Length);
                    imageFile.Flush();
                }
                return new Attached(){
                    Name = imageName,
                    FileType = FileType.Image,
                    FilePath =  $"{_pathsConfig.Complaints}{fisicalPath}{imageName}"
                };
            }catch(Exception ex){
                var x = ex;
                return null;
            }    

        }

        public async Task<Attached> SaveVideoAsync(AttachedDto attached, string dirname)
        {
            try{
                var fisicalPath = $"{dirname}/videos/";
                var upload = Path.Combine(_appEnv.WebRootPath, _pathsConfig.Complaints, fisicalPath);
                Directory.CreateDirectory(upload);

                var bytes = StringToBytesArray(attached.Data);
                using (var imageFile = new FileStream(Path.Combine(upload, attached.Name), FileMode.Create))
                {
                    await imageFile.WriteAsync(bytes, 0, bytes.Length); 
                    imageFile.Flush();
                }
                return new Attached(){
                    Name = attached.Name,
                    FileType = FileType.Video,
                    FilePath =  $"{_pathsConfig.Complaints}{fisicalPath}{attached.Name}"
                };
            }catch(Exception ex){
                var x = ex;
                return null;
            } 
        }

        public async Task<string> GetComplaintCodeAsyc()
        {
            var list = await _incrementRepository.FindBy(x => x.IncrementType == IncrementType.Complaint);
            var increment = list.FirstOrDefault();
            string code;
            if(increment != null){
                code = GetComplanintCode(increment.Number++);
                _incrementRepository.Update(increment);
                return code;
            }
            increment = new Increment();
            increment.IncrementType = IncrementType.Complaint;
            code = GetComplanintCode(increment.Number++);
            await _incrementRepository.Add(increment);
            return code;
        }

        private string GetComplanintCode(double number){
            var count = Math.Round(number).ToString();
            return $"{_townHall.Acronym}-{count.PadLeft(_townHall.ComplaintCodeDigits, '0')}";
        }

        private byte[] StringToBytesArray(string str)
        {
            var bitsToPad = 8 - str.Length % 8;

            if (bitsToPad != 8)
            {
                var neededLength = bitsToPad + str.Length;
                str = str.PadLeft(neededLength, '0');
            }

            int size= str.Length / 8;
            byte[] arr = new byte[size];

            for (int a = 0; a < size; a++)
            {
                arr[a] = Convert.ToByte(str.Substring(a * 8, 8), 2);
            }

            return arr;
        }

        public async Task<bool> SendBroadCastStatusChange(string code, StatusComplaint statusComplaint)
        {
            bool isSuccess = false;
            switch(statusComplaint){
                case StatusComplaint.Send: 
                    isSuccess = await _broadCastService.SendBroadCastAsync(new BroadCast(){Title=$"Pendiente! denuncia {code}", Message="El personal de ASDE ha cambiado el estado de su denuncia a pendiente."});
                    break;
                case StatusComplaint.InProcess:
                    isSuccess = await _broadCastService.SendBroadCastAsync(new BroadCast(){Title=$"En Proceso! denuncia {code}", Message="El personal de ASDE se ha puesto en marcha para solucionar su denuncia."});
                    break;
                case StatusComplaint.Attended: 
                    isSuccess = await _broadCastService.SendBroadCastAsync(new BroadCast(){Title=$"Atendida! denuncia {code}", Message="El personal de ASDE ha atendido a su denuncia!"});
                    break;
            }
            return isSuccess;
        }
    }
}