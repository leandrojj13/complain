using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Complain.BL.Abstract;
using Complain.Core.Models;
using Complain.DataModel.Entities;
using Complain.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace Complain.Services.Implementation
{
    public class ComplaintTypeService : IComplaintTypeService{
        private readonly IComplaintTypeRepository _complaintTypeRepository;
        private readonly PathsConfig _pathsConfig;
        private readonly IHostingEnvironment _appEnv;

        public ComplaintTypeService(IComplaintTypeRepository complaintTypeRepository,
                                    IOptions<PathsConfig> pathsConfig,
                                    IHostingEnvironment appEnv){
            _complaintTypeRepository = complaintTypeRepository;
            _pathsConfig = pathsConfig.Value;
            _appEnv = appEnv;
        }

        public async Task<string> SaveImageAsync(IFormFile image, string fileName)
        {
            if (image != null && image.Length > 0)
            {
                var file = image;
                var uploads = Path.Combine(_appEnv.WebRootPath, _pathsConfig.ImageComplaintsTypes);
                Directory.CreateDirectory(uploads);
                
                var fileNameWithExtesion = $"{fileName}{Path.GetExtension(file.FileName)}";
                using (var fileStream = new FileStream(Path.Combine(uploads, fileNameWithExtesion), FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
                return $"{_pathsConfig.ApiUrl}{_pathsConfig.ImageComplaintsTypes}{fileNameWithExtesion}";
            }
            return $"{_pathsConfig.ApiUrl}{_pathsConfig.DefaultMediaContent.ComplaintType}";
        }
    }
}