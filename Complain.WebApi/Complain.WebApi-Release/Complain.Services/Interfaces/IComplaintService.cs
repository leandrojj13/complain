using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Complain.BL.Dto;
using Complain.DataModel.Entities;
using Complain.DataModel.Entities.Enums;

namespace Complain.Services.Interfaces
{
   public interface IComplaintService
   {
        Task<IEnumerable<Attached>> SaveAttachmentsAsync(List<AttachedDto> attachmentsDto, string dirname);
        Task<Attached> SaveImageAsync(AttachedDto attached, string dirname);
        Task<Attached> SaveVideoAsync(AttachedDto attached, string dirname);
        Task<Attached> SaveAudioAsync(AttachedDto attached, string dirname);
        Task<bool> SendBroadCastStatusChange(string code, StatusComplaint statusComplaint);
        Task<string> GetComplaintCodeAsyc();
   }
}
