using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Complain.BL.Dto;
using Complain.DataModel.Entities;

namespace Complain.Services.Interfaces
{
   public interface IBroadCastService
   {
        Task<bool> SendBroadCastAsync(BroadCast broadCast);
   }
}
