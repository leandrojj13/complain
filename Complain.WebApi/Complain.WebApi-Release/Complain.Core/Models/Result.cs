
using System.Net;

namespace Complain.Core.Models
{
    public class Result<T> : Result where T : class
    {
        public T Entity { get; set; }
    }

    public class Result
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public Result()
        {
            Success = false;
            Message = "";
            StatusCode = 0;
        }
    }
}