using System.Collections.Generic;
using System.Net;

namespace Complain.Core.Models
{
    public class GetManyResult<T>  where T : class
    {
        public IEnumerable<T> Entities { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public GetManyResult()
        {
            Success = false;
            Message = "";
            StatusCode = 0;
        }
    }
}