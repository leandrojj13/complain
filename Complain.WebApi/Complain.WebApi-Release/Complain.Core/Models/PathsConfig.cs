using System.Collections.Generic;
using System.Net;

namespace Complain.Core.Models
{
    public class PathsConfig 
    {
        public string ApiUrl { get; set; }
        public string AdminUrl { get; set; }
        public string ImageComplaintsTypes { get; set; }
        public string Complaints { get; set; }
        public DefaultMediaContent DefaultMediaContent { get; set; }
    }
}