using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Complain.Core.Models;

namespace Complain.Core
{
    public interface IEntityBaseRepository<T> where T : class, IEntityBase, new()
    {
        Task<IEnumerable<T>> GetAll(params Expression<Func<T, object>>[] includeProperties);
        Task<T> Find(Guid id, params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> FindBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        Task<Result> Add(T value);
        Task Save();
        IEnumerable<Result> AddRange(IEnumerable<T> values);
        Result Update(T value);
        Result Remove(T value);
    }
}