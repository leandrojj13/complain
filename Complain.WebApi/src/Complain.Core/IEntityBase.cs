using System;

namespace Complain.Core{
    public interface IEntityBase
    {
        Guid Id { get; set; }
        bool Deleted { get; set; }
        
    }

    public interface IAuditEntityBase : IEntityBase
    {
        DateTimeOffset CreatedDate { get; set; }
        DateTimeOffset? ModifiedDate { get; set; }
        Guid CreatedBy { get; set; }
        Guid? ModifiedBy { get; set; }
        bool Disabled { get; set; }
    }
}