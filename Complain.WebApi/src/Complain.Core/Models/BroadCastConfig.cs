using System.Collections.Generic;
using System.Net;

namespace Complain.Core.Models
{
    public class BroadCastConfig{
        public string ApiToken { get; set; }
        public string ApiUrl { get; set; }
    }
}