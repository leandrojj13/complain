using System.Collections.Generic;
using System.Net;

namespace Complain.Core.Models
{
    public class TownHall{
        public string Name { get; set; }
        public string Acronym { get; set; }
        public string Province { get; set; }
        public int ComplaintCodeDigits { get; set; }
    }
}