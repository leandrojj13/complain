using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using Complain.Core;
using Complain.Core.Extensions;
using Complain.Core.Models;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Complain.Core.Repositories
{
    public class EntityBaseRepository<T> : IEntityBaseRepository<T> where T : class, IEntityBase, new()
    {
        private readonly DbContext _context;
        public DbSet<T> _set;
        public IValidator<T> _validator;

        public EntityBaseRepository(DbContext context, IValidator<T> validator)
        {
            _context = context;
            _set = context.Set<T>();
            _validator = validator;
        }

        public virtual async Task<Result> Add(T entity)
        {
            var results = _validator.Validate(entity);
            if (results.IsValid)
            {
                var entry = await _set.AddAsync(entity);
                return new Result() { Success = true, StatusCode = HttpStatusCode.Created };
            }
            var errosMsg = results.Errors.ToMessage();
            return new Result() { StatusCode = HttpStatusCode.BadRequest, Message = errosMsg };
        }

        public virtual IEnumerable<Result> AddRange(IEnumerable<T> entityEnumerable)
        {
            foreach (var ent in entityEnumerable)
            {
                var results = _validator.Validate(ent);
                if (!results.IsValid)
                {
                    var errosMsg = results.Errors.ToMessage();
                    yield return new Result() { StatusCode = HttpStatusCode.BadRequest, Message = errosMsg };
                }
            }
            _set.AddRange(entityEnumerable);
            yield return new Result() { StatusCode = HttpStatusCode.Created, Success = true };
        }

        public virtual async Task<T> Find(Guid id, params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _set.Where(x => x.Deleted == false);
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return await query.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
        }
        public virtual async Task<IEnumerable<T>> FindBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
           IQueryable<T> query = _set.Where(predicate);
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty).Where(y=> !y.Deleted);
            }
            return await query.AsNoTracking().ToListAsync();
        }

        public virtual async Task<IEnumerable<T>> GetAll(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _set.Where(x => x.Deleted == false);
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return await query.ToListAsync();
        }

        public virtual Result Remove(T entity)
        {
            EntityEntry dbEntityEntry = _context.Entry(entity);
            entity.Deleted = true;
            dbEntityEntry.State = EntityState.Modified;
            return new Result() { StatusCode = HttpStatusCode.OK, Success = true };
        }

        public virtual Result Update(T entity)
        {
            var results = _validator.Validate(entity);
            if (results.IsValid)
            {
                EntityEntry dbEntityEntry = _context.Entry(entity);
                dbEntityEntry.State = EntityState.Modified;
                return new Result() { StatusCode = HttpStatusCode.OK, Success = true };
            }
            var errosMsg = results.Errors.ToMessage();
            return new Result() { StatusCode = HttpStatusCode.BadRequest, Message = errosMsg };
        }

        public virtual async Task Save()
        {
            try
            {
                CreateAuditLogInformation();
                ModifyAuditInformation();
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException /* ex */)
            {
                //Log the error (uncomment ex variable name and write a log.)
                //ModelState.AddModelError("", "Unable to save changes. " +
                //"Try again, and if the problem persists, " +
                // "see your system administrator.");
            }
        }

        private void CreateAuditLogInformation()
        {
            var entitiesAdded = _context.ChangeTracker.Entries<IAuditEntityBase>()
                                .Where(x => x.State == EntityState.Added);
            foreach (var entry in entitiesAdded)
            {
                entry.Entity.CreatedBy = Guid.Empty;
                entry.Entity.CreatedDate = DateTime.Now;
            }
        }

        private void ModifyAuditInformation()
        {
            var entitiesModified = _context.ChangeTracker.Entries<IAuditEntityBase>()
                                    .Where(x => x.State == EntityState.Modified);
            foreach (var entry in entitiesModified)
            {
                entry.Entity.ModifiedBy = Guid.Empty;
                entry.Entity.ModifiedDate = DateTime.Now;
            }
        }
        
    }
}