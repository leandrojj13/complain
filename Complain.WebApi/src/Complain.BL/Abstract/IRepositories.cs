using Complain.Core;
using Complain.DataModel.Entities;

namespace Complain.BL.Abstract{
    public interface IComplaintTypeRepository : IEntityBaseRepository<ComplaintType> { }
    public interface IComplaintRepository : IEntityBaseRepository<Complaint> { }
    public interface IIncrementRepository : IEntityBaseRepository<Increment> { }
    public interface IBroadCastRepository : IEntityBaseRepository<BroadCast> { }
    // public interface IUserRepository : IEntityBaseRepository<ApplicationUser> { }
}