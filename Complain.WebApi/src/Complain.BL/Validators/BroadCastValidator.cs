using Complain.DataModel.Entities;
using FluentValidation;

namespace Complain.BL.Validators{
    public class BroadCastValidator :  AbstractValidator<BroadCast>{
        
        public BroadCastValidator(){
            RuleFor(broadCast => broadCast.Title).NotNull()
                                              .NotEmpty();
            RuleFor(broadCast => broadCast.Message).NotNull()
                                                .NotEmpty();
        }
    }
}