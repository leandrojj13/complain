using Complain.BL.Abstract;
using Complain.Core.Repositories;
using Complain.DataModel.Context;
using Complain.DataModel.Entities;
using FluentValidation;

namespace Complain.BL.Repositories
{
    public class IncrementRepository : EntityBaseRepository<Increment>, IIncrementRepository
    {
        public IncrementRepository(ComplainDbContext context, IValidator<Increment> validator) 
        : base(context, validator){

        }
    }
}