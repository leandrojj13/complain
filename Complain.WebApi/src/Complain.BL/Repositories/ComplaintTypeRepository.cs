using Complain.BL.Abstract;
using Complain.Core.Repositories;
using Complain.DataModel.Context;
using Complain.DataModel.Entities;
using FluentValidation;

namespace Complain.BL.Repositories
{
    public class ComplaintTypeRepository : EntityBaseRepository<ComplaintType>, IComplaintTypeRepository
    {
        public ComplaintTypeRepository(ComplainDbContext context, IValidator<ComplaintType> validator) 
        : base(context, validator){

        }
    }
}