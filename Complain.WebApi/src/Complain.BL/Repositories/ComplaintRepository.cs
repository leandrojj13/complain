using Complain.BL.Abstract;
using Complain.Core.Repositories;
using Complain.DataModel.Context;
using Complain.DataModel.Entities;
using FluentValidation;

namespace Complain.BL.Repositories
{
    public class ComplaintRepository : EntityBaseRepository<Complaint>, IComplaintRepository
    {
        public ComplaintRepository(ComplainDbContext context, IValidator<Complaint> validator) 
        : base(context, validator){

        }
    }
}