using Complain.BL.Abstract;
using Complain.Core.Repositories;
using Complain.DataModel.Context;
using Complain.DataModel.Entities;
using FluentValidation;

namespace Complain.BL.Repositories
{
    public class BroadCastRepository : EntityBaseRepository<BroadCast>, IBroadCastRepository
    {
        public BroadCastRepository(ComplainDbContext context, IValidator<BroadCast> validator) 
        : base(context, validator){

        }
    }
}