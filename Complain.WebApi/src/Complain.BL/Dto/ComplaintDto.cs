using System;
using System.Collections.Generic;
using Complain.BL.Abstract;
using Complain.DataModel.Entities.Enums;

namespace Complain.BL.Dto
{
    public class ComplaintDto : IEntityBaseDto
    {
        public string Id { get; set; }
        public string Codigo { get; set; }
        public string Description { get; set; }
        public StatusComplaint Status { get; set; }
        public string UserId { get; set; }
        public string ComplaintTypeId { get; set; }
        public string ComplaintTypeName { get; set; }
        public string ComplaintTypeImageUrl { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public string Sector { get; set; }
        public string Street { get; set; }
        public string AddressDescription { get; set; }
        public string Residencia { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public AddressType AddressType { get; set; }
        public List<AttachedDto> Attachments { get; set; }
        
    }

}