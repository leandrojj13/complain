using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Complain.BL.Dto;
using Complain.Core.Models;
using Complain.DataModel.Context;
using Complain.DataModel.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Complain.Api.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ComplainDbContext _dbContext;
        protected readonly IMapper _mapper;

        public AccountController(
            UserManager<ApplicationUser> userManager, 
            IMapper mapper,
            ComplainDbContext applicationDbContext)
        {
            _userManager = userManager;
            _dbContext = applicationDbContext;
            _mapper = mapper;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetAll()
        {
            var users = _dbContext.Users.ToList();
      
            var response = new GetManyResult<UserDto>()
            {
                Entities = _mapper.Map<List<UserDto>>(users),
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(response);
        }

        [HttpGet("[action]")]
        [AllowAnonymous]
        public async Task<IActionResult> UserInfo(string email)
        {
            if(email == null){
                return BadRequest();
            }
            var user = await _userManager.FindByNameAsync(email);
            if (user != null)
            {
                var response = new Result<UserDto>()
                {
                    Entity = _mapper.Map<UserDto>(user),
                    Success = true,
                    StatusCode = HttpStatusCode.OK
                };
                return Ok(response);
            }
            // If we got this far, something failed.
            return BadRequest(new Result{
                StatusCode = HttpStatusCode.BadRequest,
                Message = "Usuario no encontrado!"
            });
        }
        
        // POST: /Account/Register
        [HttpPost("[action]")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] RegisterDto model)
        {
            var user = await _userManager.FindByNameAsync(model.Email);
            if (user != null)
            {
                var response = new Result<UserDto>()
                {
                    Entity = _mapper.Map<UserDto>(user),
                    StatusCode = HttpStatusCode.Conflict,
                    Message = "Este usuario existe!"
                };
                return BadRequest(response);
            }
            user = _mapper.Map<ApplicationUser>(model);
            
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                var responseSucceeded = new Result<UserDto>()
                {
                    Entity = _mapper.Map<UserDto>(user),
                    Success = true,
                    StatusCode = HttpStatusCode.OK
                };
                return Ok(responseSucceeded);
            }
            var responseFaild = new GetManyResult<string>()
            {
                Message = result.Errors.Select(x=>x.Description).FirstOrDefault(),
                StatusCode = HttpStatusCode.BadRequest
            };
            // If we got this far, something failed.
            return BadRequest(responseFaild);
        }

                // POST: /Account/Register
        [HttpPost("[action]")]
        [AllowAnonymous]
        public async Task<IActionResult> CheckUserExist([FromBody] string email)
        {
            if(email == null){
                return BadRequest();
            }
            
            var user = await _userManager.FindByNameAsync(email);
            if (user != null)
            {
                var response = new Result<UserDto>()
                {
                    Entity = _mapper.Map<UserDto>(user),
                    StatusCode = HttpStatusCode.BadRequest,
                    Message = "Este usuario existe!"
                };
                return BadRequest(response);
            }
            var responseSucceeded = new Result<UserDto>()
            {
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(responseSucceeded); 
        }
    }
}