using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AutoMapper;
using Complain.BL.Abstract;
using Complain.BL.Dto;
using Complain.Core.Models;
using Complain.DataModel.Entities;
using Complain.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Complain.Api.Controllers.Api
{
    // [Authorize]
    public class ComplaintTypeController : BaseApiController<ComplaintType, ComplaintTypeDto>
    {
        private readonly IComplaintTypeRepository _complaintTypeRepository;
        private readonly IComplaintTypeService _complaintTypeService;

        public ComplaintTypeController(IComplaintTypeRepository complaintTypeRepository,
                                      IComplaintTypeService complaintTypeService, IMapper mapper)
            : base(complaintTypeRepository, mapper)
        {
            _complaintTypeRepository = complaintTypeRepository;
            _complaintTypeService = complaintTypeService;
        }


        // POST api/values/
        [HttpPost("[action]")]
        public async Task<IActionResult> Create(IFormCollection form)
        {
            var modelJSON = form["model"];
            var dto = JsonConvert.DeserializeObject<ComplaintTypeDto>(modelJSON);

            var model = _mapper.Map<ComplaintType>(dto);

            var resultRepository = await _db.Add(model);
            if (!resultRepository.Success)
                return Ok(resultRepository);

            model.ImageUrl = await _complaintTypeService.SaveImageAsync(form.Files[0], model.Id.ToString("D"));
            await _db.Save();

            var result = new Result<ComplaintTypeDto>()
            {
                Entity = _mapper.Map<ComplaintTypeDto>(model),
                Success = true,
                StatusCode = HttpStatusCode.Created
            };
            return CreatedAtAction("Get", new { id = model.Id }, result);
        }

        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Update(string id, IFormCollection form)
        {
            var modelJSON = form["model"];


            var dto = JsonConvert.DeserializeObject<ComplaintTypeDto>(modelJSON);

            Guid guidID = Guid.Empty;
            if (dto == null || !Guid.TryParse(id, out guidID))
            {
                return BadRequest(new Result() { StatusCode = HttpStatusCode.BadRequest });
            }

            var find = await _db.Find(Guid.Parse(id));
            if (find == null)
            {
                return NotFound(new Result() { StatusCode = HttpStatusCode.NotFound });
            }

            var model = _mapper.Map<ComplaintType>(dto);

            if (form.Files.Count > 0)
            {
                var image = form.Files[0];
                if (image != null)
                    model.ImageUrl = await _complaintTypeService.SaveImageAsync(image, model.Id.ToString("D"));
            }


            var result = _db.Update(model);
            if (result.Success)
                await _db.Save();

            return Ok(result);
        }
    }
}
