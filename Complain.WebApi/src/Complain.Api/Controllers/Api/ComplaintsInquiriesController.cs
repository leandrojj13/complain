using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Complain.BL.Abstract;
using Complain.BL.Dto;
using Complain.Core.Models;
using Complain.DataModel.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System;
using Complain.Services.Interfaces;
using Complain.DataModel.Entities.Enums;

namespace Complain.Api.Controllers.Api
{
    // [Authorize]
    public partial class ComplaintController 
    {
        
        [HttpGet("[action]")]
        public async Task<IActionResult> FilterByAttended()
        {
            var complaints = await _db.FindBy(x => x.Status == StatusComplaint.Attended, y => y.ComplaintType);

            var dto = _mapper.Map<List<ComplaintDto>>(complaints);
            var result = new
            {
                Entities = dto,
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(result);
        }
        [HttpGet("[action]")]
        public async Task<IActionResult> FilterByPending()
        {
            var complaints = await _db.FindBy(x => x.Status == StatusComplaint.Send, y => y.ComplaintType);
            var dto = _mapper.Map<List<ComplaintDto>>(complaints);
            var result = new
            {
                Entities = dto,
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(result);
        }
        [HttpGet("[action]")]
        public async Task<IActionResult> FilterByToday()
        {
            var complaints = await _db.FindBy(x => x.CreatedDate.ToString("dd/MM/yyyy") == DateTime.Today.ToString("dd/MM/yyyy") && x.Status == StatusComplaint.Send, y => y.ComplaintType);
            var dto = _mapper.Map<List<ComplaintDto>>(complaints);
            var result = new
            {
                Entities = dto,
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(result);
        }
        [HttpGet("[action]")]
        public async Task<IActionResult> FilterByRecent()
        {
            var complaints = await _db.FindBy(x => x.Status == StatusComplaint.Send, y => y.ComplaintType);
            var lastTen = complaints.Take(10);
            var dto = _mapper.Map<List<ComplaintDto>>(lastTen);
            var result = new
            {
                Entities = dto,
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(result);
        }
        [HttpGet("[action]")]
        public async Task<IActionResult> FilterByLastWeek()
        {
            var lastWeek = DateTime.Today.AddDays(-7).Date;
            var complaints = await _db.FindBy(x => x.CreatedDate.Date >= lastWeek && x.Status == StatusComplaint.Send, y => y.ComplaintType);
            var dto = _mapper.Map<List<ComplaintDto>>(complaints);
            var result = new
            {
                Entities = dto,
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(result);
        }

        [HttpGet("[action]/{code}")]
        public async Task<IActionResult> FilterByCode(string code)
        {
            var complaints = await _db.FindBy(x => x.Codigo.ToString().Contains(code), y => y.ComplaintType);
            var dto = _mapper.Map<List<ComplaintDto>>(complaints);
            var result = new
            {
                Entities = dto,
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(result);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetComplaintsMadeBarChart(DateTime? filterDate)
        {
            var complaintsMade = await _complaintsInquiriesService.GetComplaintsMadeBarChart(filterDate);
            var result = new
            {
                Entities = complaintsMade,
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(result);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetComplaintsMadeLineChartMonthlyByYear(int year)
        {
            var complaintsMade = await _complaintsInquiriesService.GetComplaintsMadeLineChartMonthlyByYear(year);
            var result = new
            {
                Entities = complaintsMade,
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(result);
        }


        [HttpGet("[action]")]
        public async Task<IActionResult> GetComplaintsMadeLineChartAnual()
        {
            var complaintsMade = await _complaintsInquiriesService.GetComplaintsMadeLineChartAnnual();
            var result = new
            {
                Entities = complaintsMade,
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(result);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetYearsWhereComplaintsWereMade()
        {
            var complaints = await _db.GetAll();
            var years =
                complaints.GroupBy(z => z.CreatedDate.Year)
                          .Select(f =>
                          {
                              return f.FirstOrDefault().CreatedDate.Year;
                          }).ToList<int>();

            years.Sort();
            var result = new
            {
                Entities = years,
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(result);
        }
    }
}
