using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Complain.BL.Abstract;
using Complain.BL.Dto;
using Complain.Core.Models;
using Complain.DataModel.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System;
using Complain.Services.Interfaces;
using Complain.DataModel.Entities.Enums;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.IO;

namespace Complain.Api.Controllers.Api
{
    // [Authorize]
    public partial class ComplaintController : BaseApiController<Complaint, ComplaintDto>
    {
        private readonly IComplaintService _complaintService;
        private readonly IComplaintsInquiriesService _complaintsInquiriesService;
        private readonly UserManager<ApplicationUser> _userManager;

        public ComplaintController(IComplaintRepository complaintRepository,
                                   IMapper mapper,
                                   UserManager<ApplicationUser> userManager, 
                                   IComplaintService complaintService,
                                   IComplaintsInquiriesService complaintsInquiriesService)
            : base(complaintRepository, mapper)
        {
            _userManager = userManager;
            _complaintService = complaintService;
            _complaintsInquiriesService = complaintsInquiriesService;
        }
        //POST api/values/
        [HttpPost]
        public override async Task<IActionResult> Create([FromBody] ComplaintDto dto)
        {
            var model = _mapper.Map<Complaint>(dto);

            // var user = await _userManager.GetUserAsync(User);
            // if (user == null)
            // {
            //     return BadRequest("Usuario no autenticado");
            // }
          //  model.UserId = new Guid("57B96A47-A7B5-4330-BAEF-0D8FA5741E68");

            var resultRepository = await _db.Add(model);
            if (!resultRepository.Success)
                return BadRequest(resultRepository);

            var attachments = await _complaintService.SaveAttachmentsAsync(dto.Attachments, model.Id.ToString("D"));

            model.Attachments.AddRange(attachments);
            model.Codigo = await _complaintService.GetComplaintCodeAsyc();

            await _db.Save();
            var result = new Result<ComplaintDto>()
            {
                Entity = _mapper.Map<ComplaintDto>(model),
                Success = true,
                StatusCode = HttpStatusCode.Created
            };
            return CreatedAtAction("Get", new { id = model.Id }, result);
        }

        // GET: api/values
        [HttpGet]
        public override async Task<IActionResult> GetAll()
        {
            var list = await _db.GetAll(x => x.ComplaintType);
            var listDto = _mapper.Map<List<ComplaintDto>>(list);
            var result = new GetManyResult<ComplaintDto>()
            {
                Entities = listDto,
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(result);
        }

        // GET api/values/5 
        [HttpGet("{id}")]
        public override async Task<IActionResult> Get(Guid id)
        {
            var model = await _db.Find(id, x => x.ComplaintType, y => y.Attachments);

            if (model == null)
            {
                return NotFound(new Result() { StatusCode = HttpStatusCode.NotFound });
            }

            var dto = _mapper.Map<ComplaintDto>(model);
            var result = new Result<ComplaintDto>()
            {
                Entity = dto,
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(result);
        }

        [HttpGet("[action]/{id}/{status}")]
        public async Task<IActionResult> ChangeStatus(string id, StatusComplaint status)
        {
            var complaint = await _db.Find(Guid.Parse(id), x => x.ComplaintType);
            if (complaint == null)
            {
                return NotFound(new Result() { StatusCode = HttpStatusCode.NotFound });
            }
            complaint.Status = status;

            var isSuccess = await _complaintService.SendBroadCastStatusChange(complaint.Codigo, status);

            if(!isSuccess)
            return BadRequest(new Result() { Message = "Error al enviar notificación, el estado de la denuncia no se ha podido cambiar.", StatusCode = HttpStatusCode.BadRequest});

            var result = _db.Update(complaint);
            if (result.Success)
            {
                await _db.Save();
            }
            var dto = _mapper.Map<ComplaintDto>(complaint);
            var resultDto = new
            {
                Entity = dto,
                Success = true,
                StatusCode = HttpStatusCode.OK
            };
            return Ok(resultDto);
        }

    }
}
