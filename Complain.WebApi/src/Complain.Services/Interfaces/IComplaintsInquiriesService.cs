using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Complain.Services.Interfaces
{
   public interface IComplaintsInquiriesService
   {
        Task<object> GetComplaintsMadeBarChart(DateTime? filterDate);
        Task<object> GetComplaintsMadeLineChartMonthlyByYear(int year);
        Task<object> GetComplaintsMadeLineChartAnnual();
   }
}
