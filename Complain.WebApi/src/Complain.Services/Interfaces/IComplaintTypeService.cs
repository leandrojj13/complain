using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Complain.Services.Interfaces
{
   public interface IComplaintTypeService
   {
        Task<string> SaveImageAsync(IFormFile image, string fileName);
   }
}
