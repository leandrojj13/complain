using System;
using System.ComponentModel.DataAnnotations.Schema;
using Complain.Core;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Complain.DataModel.Entities{

    public class ApplicationUser : IdentityUser<Guid>
    {
         public string GivenName { get; set; }
        // [Column("UserId")]
        // public Guid Id { get; set; }
        // public string Name { get; set; }
        // public string LastName { get; set; }
        // public string Email { get; set; }
        // public string ImgProfile { get; set; }
        // public string Password { get; set; }
        // public string PhoneNumber { get; set; }

        // public DateTimeOffset CreatedDate { get; set; }
        // public DateTimeOffset? ModifiedDate { get; set; }
        // public Guid CreatedBy { get; set; }
        // public Guid? ModifiedBy { get; set; }
        // public bool Disabled { get; set; }
        // public bool Deleted { get; set; }

        // public Guid RolId { get; set; }
        // public virtual Rol Rol { get; set; }
    }

}
