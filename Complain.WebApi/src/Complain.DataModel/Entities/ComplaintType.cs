using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Complain.Core;

namespace Complain.DataModel.Entities{
    public class ComplaintType : IAuditEntityBase
    {
        public ComplaintType(){
            Complaints = new HashSet<Complaint>();
        }

        [Column("ComplaintTypeId")]
        public Guid Id { get; set; }
        [Required]
        public string Codigo { get; set; }
        [Required]
        [StringLength(20, MinimumLength=5)]
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        [StringLength(50, MinimumLength=5)]
        public string Description { get; set; }

        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public bool Disabled { get; set; }
        public bool Deleted { get; set; }
        public virtual HashSet<Complaint> Complaints { get; set; }
    }
}