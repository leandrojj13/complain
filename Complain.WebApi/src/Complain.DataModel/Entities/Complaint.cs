using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Complain.Core;
using Complain.DataModel.Entities.Enums;

namespace Complain.DataModel.Entities{
    public class Complaint : IAuditEntityBase
    {
        public Complaint(){
            Attachments = new List<Attached>();
        }
        [Column("ComplaintId")]
        public Guid Id { get; set; }
        [Required]
        public string Codigo { get; set; }
        [Required]
        [StringLength(int.MaxValue, MinimumLength=10)]
        public string Description { get; set; }
        [Required]
        public StatusComplaint Status { get; set; }
        public string Sector { get; set; }
        public string Street { get; set; }
        public string AddressDescription { get; set; }
        public string Residencia { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        [Required]
        public AddressType AddressType { get; set; }

        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public bool Disabled { get; set; }
        public bool Deleted { get; set; }

        public Guid ComplaintTypeId { get; set; }
        public Guid UserId { get; set; }
        public virtual ComplaintType ComplaintType { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual  List<Attached> Attachments { get; set; }
    }
}