using System;
using System.Linq;
using Complain.DataModel.Entities;
using Complain.DataModel.SampleData;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace Complain.DataModel.Context
{
    public static class DbInitializer
    {
        public async static void Initialize(ComplainDbContext context, UserManager<ApplicationUser> _userManager, IConfigurationRoot Configuration)
        {
            //context.Database.EnsureCreated();

            if (!context.Users.Any())
            {
               await _userManager.CreateAsync(UserSample.Users.FirstOrDefault(user => user.GivenName == "Administrador"), "Admin1234,");
               await _userManager.CreateAsync(UserSample.Users.FirstOrDefault(user => user.GivenName == "Leandro Jimenez"), "Admin1234,");
               await _userManager.CreateAsync(UserSample.Users.FirstOrDefault(user => user.GivenName == "José Aquino"), "Admin1234,");
            }

            if (!context.ComplaintsTypes.Any())
            {
                var defaultImageUrl = $"{Configuration["PathsConfig:ApiUrl"]}{Configuration["PathsConfig:DefaultMediaContent:ComplaintType"]}";
                ComplaintTypeSample.ComplaintsTypes.ForEach(ComplaintType => ComplaintType.ImageUrl = defaultImageUrl);
                context.ComplaintsTypes.AddRange(ComplaintTypeSample.ComplaintsTypes);
                await context.SaveChangesAsync();
            }

            if (!context.Complaints.Any())
            {
                var userId = UserSample.Users.FirstOrDefault(user => user.GivenName == "Administrador").Id;
                ComplaintSampleSample.Complaints.ForEach(Complaint=> Complaint.UserId = userId);
                context.Complaints.AddRange(ComplaintSampleSample.Complaints);
                await context.SaveChangesAsync();
            }

            if (!context.Increments.Any())
            {
                context.Increments.AddRange(IncrementSample.Increments);
                await context.SaveChangesAsync();
            }
        }
    }
}