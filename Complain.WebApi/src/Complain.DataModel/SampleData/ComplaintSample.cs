using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Complain.Core;
using Complain.DataModel.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Complain.DataModel.SampleData{
    public class ComplaintSampleSample  
    {
        private static List<Complaint> _complaints;

        static ComplaintSampleSample()
        {
            if (_complaints == null)
            {
                Complaints = new List<Complaint>()
                {
                    new Complaint{ Id=Guid.NewGuid(), Codigo="ASDE-PRUEBA1", ComplaintTypeId= ComplaintTypeSample.ComplaintsTypes.FirstOrDefault(x => x.Codigo == "VRTDR").Id, Description="La basura no pasa vengan rapido por favor.", CreatedDate = DateTime.Now, CreatedBy = Guid.Empty },
                    new Complaint{ Id=Guid.NewGuid(), Codigo="ASDE-PRUEBA2", ComplaintTypeId= ComplaintTypeSample.ComplaintsTypes.FirstOrDefault(x => x.Codigo == "VRTDR").Id, Description="El barrio esta muy contaminado pueden mandar el camion.", CreatedDate = DateTime.Now, CreatedBy = Guid.Empty },
                    new Complaint{ Id=Guid.NewGuid(), Codigo="ASDE-PRUEBA3", ComplaintTypeId= ComplaintTypeSample.ComplaintsTypes.FirstOrDefault(x => x.Codigo == "VRTDR").Id, Description="Lugar donde se almacena una gran cantidad de basura es nuestro hogar y ustedes no pasan a recogerla.", CreatedDate = DateTime.Now, CreatedBy = Guid.Empty },
                    new Complaint{ Id=Guid.NewGuid(), Codigo="ASDE-PRUEBA4", ComplaintTypeId= ComplaintTypeSample.ComplaintsTypes.FirstOrDefault(x => x.Codigo == "ABLCID").Id, Description="Arbol esta caida y nadie viene a recogerlo.", CreatedDate = DateTime.Now, CreatedBy = Guid.Empty },
                    new Complaint{ Id=Guid.NewGuid(), Codigo="ASDE-PRUEBA5", ComplaintTypeId= ComplaintTypeSample.ComplaintsTypes.FirstOrDefault(x => x.Codigo == "SMFRDD").Id, Description="El semaforo esta averiado y hay tapones en esta zona.", CreatedDate = DateTime.Now, CreatedBy = Guid.Empty },
                    new Complaint{ Id=Guid.NewGuid(), Codigo="ASDE-PRUEBA6", ComplaintTypeId= ComplaintTypeSample.ComplaintsTypes.FirstOrDefault(x => x.Codigo == "SMFRDD").Id, Description="Muchos tapones, arreglen el semaforo por favor.", CreatedDate = DateTime.Now, CreatedBy = Guid.Empty },
                    new Complaint{ Id=Guid.NewGuid(), Codigo="ASDE-PRUEBA7", ComplaintTypeId= ComplaintTypeSample.ComplaintsTypes.FirstOrDefault(x => x.Codigo == "CLLNALT").Id, Description="La calle esta muy mala, deberian venir a asfaltarla", CreatedDate = DateTime.Now, CreatedBy = Guid.Empty },
                    new Complaint{ Id=Guid.NewGuid(), Codigo="ASDE-PRUEBA8", ComplaintTypeId= ComplaintTypeSample.ComplaintsTypes.FirstOrDefault(x => x.Codigo == "PRQDCI").Id, Description="El parque Rosita de salvador esta en muy malas condificones", CreatedDate = DateTime.Now, CreatedBy = Guid.Empty },
                    new Complaint{ Id=Guid.NewGuid(), Codigo="ASDE-PRUEBA9", ComplaintTypeId= ComplaintTypeSample.ComplaintsTypes.FirstOrDefault(x => x.Codigo == "ESCBRO").Id, Description="La calle esta muy mala, hay muchos escobros por todas partes", CreatedDate = DateTime.Now, CreatedBy = Guid.Empty },
                    new Complaint{ Id=Guid.NewGuid(), Codigo="ASDE-PRUEBA10", ComplaintTypeId= ComplaintTypeSample.ComplaintsTypes.FirstOrDefault(x => x.Codigo == "DRNJTPAD").Id, Description="La ovando con albert tomas tiene el drenaje tapado", CreatedDate = DateTime.Now, CreatedBy = Guid.Empty },
                    new Complaint{ Id=Guid.NewGuid(), Codigo="ASDE-PRUEBA11", ComplaintTypeId= ComplaintTypeSample.ComplaintsTypes.FirstOrDefault(x => x.Codigo == "OTROS").Id, Description="La luz tiene muy malas condiciones", CreatedDate = DateTime.Now, CreatedBy = Guid.Empty },
                    new Complaint{ Id=Guid.NewGuid(), Codigo="ASDE-PRUEBA12", ComplaintTypeId= ComplaintTypeSample.ComplaintsTypes.FirstOrDefault(x => x.Codigo == "OTROS").Id, Description="Bastante delicunencia descontrolada", CreatedDate = DateTime.Now, CreatedBy = Guid.Empty },
                
                };
            }
        }

        public static List<Complaint> Complaints { get => _complaints; set => _complaints = value; }

    }
}