import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Register3 } from '../register-3/register-3';
import { AuthService } from '../../providers/auth-service';

@Component({
  selector: 'page-register-2',
  templateUrl: 'register-2.html',
})
export class Register2 {
  form: FormGroup;
  submitAttempt: boolean;
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public authService: AuthService, 
              private alertCtrl: AlertController) {

    this.form = new FormGroup(
      {
        email: new FormControl('', Validators.email)
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Register1');
  }

  next(){
    this.submitAttempt = true;

    if(this.form.valid){
        this.authService.user.email = this.form.value.email;
      
        this.authService.CheckUserExist((response)=>{
          this.navCtrl.push(Register3);   
        },
        (response)=>{
          let alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: response.message,
            buttons: ['Ok']
          });
          alert.present();
        });   
    }
  }
}
