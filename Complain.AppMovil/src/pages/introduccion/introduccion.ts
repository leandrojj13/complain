import { Component } from '@angular/core';
import {NavController, NavParams } from 'ionic-angular';
import { Login } from '../login/login';

@Component({
  selector: 'introduccion',
  templateUrl: 'introduccion.html',
})
export class Introduccion {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() { 
    console.log('ionViewDidLoad Welcome');
  }

  next(){
    this.navCtrl.push(Login);
  }
}

