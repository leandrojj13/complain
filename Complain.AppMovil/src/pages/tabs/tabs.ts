import { Component } from '@angular/core';

import { CameraPage } from '../camera/camera';
import { MicrophonePage } from '../microphone/microphone';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { NavController } from 'ionic-angular';
import { MediaCapture } from '@ionic-native/media-capture';
import { DenunciarTipo } from '../denunciar-tipo/denunciar-tipo';

import { HomePage } from '../home/home';
import { Attached } from '../../models/Attached';
import { FileType } from '../../models/enums/FileType';

import { DataSaver } from '../../providers/data-saver';
import { Welcome } from '../welcome/welcome';
import { AndroidPermissions } from '@ionic-native/android-permissions';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = CameraPage;
  tab3Root = MicrophonePage;

  public videoSRC = "";
  public audioSRC = "";
  public base64Image = "";

  constructor(public navCtrl: NavController, public androidPermissions: AndroidPermissions , public camera : Camera, public mediaCapture: MediaCapture, public dataSaver : DataSaver) {
      //this.navCtrl.setRoot(Welcome);
      this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, 
          this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
          this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION
      ]);
  }


  

   takePic(){
      const options : CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        mediaType: this.camera.MediaType.ALLMEDIA,
        targetHeight: 1000,
        targetWidth: 1000,
      }
  
      this.camera.getPicture(options).then(imgData =>{

          var file = new Attached();
              file.name = "Foto 1";
              file.data = `data:image/jpeg;base64,${imgData}`;
              file.url = null;
              file.FileType = FileType.Image;
          this.dataSaver.file = file;
          //this.base64Image = 'data:image/jpeg;base64,' + imgData;
          this.seguir();
          
      }, err =>{});
    }

  takeVoice(){
    this.mediaCapture.captureAudio().then(data =>{

       var url = data[0].fullPath;

        var file = new Attached();
        file.name = data[0].name;
        file.data = url;
        file.url = url;
        file.FileType = FileType.Audio;
        this.dataSaver.audio = file;

        this.seguir();
    });
  }
    
  seguir(){
    this.navCtrl.push(DenunciarTipo);
  }
}
