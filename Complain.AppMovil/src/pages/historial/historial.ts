import { Component } from '@angular/core';
import { NavController,ModalController } from 'ionic-angular';
import { Complaint } from '../../models/ComplaintModel';
import { DataSaver } from '../../providers/data-saver';
import { HistorialDetalle } from '../historialDetalle/historialDetalle';
import { BroadCastModel }  from '../../models/BroadCastModel';

@Component({
  selector: 'historial',
  templateUrl: 'historial.html'
})

export class Historial {

  complaint : Array<Complaint>;
  broadCast : Array<BroadCastModel>;
  tabActual  :string;
  constructor(public navCtrl: NavController, private dataSaver : DataSaver, private modalCtrl: ModalController) {
    this.complaint = this.dataSaver.getComplaint();
    this.broadCast = this.dataSaver.broadCasts;
    this.tabActual = "Historial";
  }
   delete(item){
      let index = this.complaint.indexOf(item);
      
      if(index > -1){
          this.complaint.splice(index, 1);
      }
    }
    viewDetail(item){
        this.dataSaver.detailView = item;
        let modal = this.modalCtrl.create(HistorialDetalle);
        modal.present();   
    }
}
