import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MediaCapture } from '@ionic-native/media-capture';
import { DenunciarTipo } from '../denunciar-tipo/denunciar-tipo';


@Component({
  selector: 'microphone',
  templateUrl: 'microphone.html'
})
export class MicrophonePage {
  
  public  audioSRC = "";

  constructor(public navCtrl: NavController, public mediaCapture: MediaCapture) {
  }

   ngAfterViewInit() {
        this.navCtrl.viewDidEnter.subscribe(
          view => console.log("Current opened view is : " + view.name)
        );
    }

  ngOnInit() {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.takeVoice();
  }
  public takeVoice(){
    this.mediaCapture.captureAudio().then(data =>{
        this.audioSRC = data[0].fullPath;
        this.seguir();
    });
  }
   seguir(){
     this.navCtrl.push(DenunciarTipo);
  }

}
