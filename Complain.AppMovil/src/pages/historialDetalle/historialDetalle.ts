import { Component } from '@angular/core';
import { NavController, ViewController  } from 'ionic-angular';
import { Complaint } from '../../models/ComplaintModel';
import { DataSaver } from '../../providers/data-saver';
import * as $  from 'jquery';

@Component({
  selector: 'historialDetalle',
  templateUrl: 'historialDetalle.html'
})

export class HistorialDetalle {
  
  detailView : Complaint;

  complaint : Array<Complaint>;

  constructor(public navCtrl: NavController, private dataSaver : DataSaver, private viewCtrl: ViewController) {
    this.detailView = this.dataSaver.detailView;
    console.log(this.dataSaver.detailView);
  }
  ngOnInit() {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
   // $("#buttonLocation").click();
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
