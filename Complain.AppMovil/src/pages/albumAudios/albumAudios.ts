import { Component } from '@angular/core';
import { NavController, ViewController  } from 'ionic-angular';
import { Complaint } from '../../models/ComplaintModel';
import { DataSaver } from '../../providers/data-saver';
import * as $  from 'jquery';
import { Attached } from '../../models/Attached';
import { StreamingMedia,  StreamingAudioOptions } from '@ionic-native/streaming-media';

@Component({
  selector: 'albumAudios',
  templateUrl: 'albumAudios.html'
})

export class AlbumAudios {
  
  public listAudio: Array<Attached> = [];

  constructor(public navCtrl: NavController, public streamingMedia: StreamingMedia, private dataSaver : DataSaver, private viewCtrl: ViewController) {
    this.listAudio = this.dataSaver.listAudio;
  }

  ngOnInit() {

  }

  playAudio(item){
    let options: StreamingAudioOptions = {
      successCallback: () => { console.log('Video played') },
      errorCallback: (e) => { console.log('Error streaming') },
    };
    this.streamingMedia.playAudio(item.url, options);
  }

  deleteAudio(item){
    let index = this.dataSaver.listAudio.indexOf(item);
    if(index > -1){
        this.dataSaver.listAudio.splice(index, 1);
    }
  }


  dismiss() {
    this.viewCtrl.dismiss();
  }
}
