import { Component } from '@angular/core';
import {NavController, NavParams } from 'ionic-angular';
import { Register1 } from '../register-1/register-1';
import { Login } from '../login/login';

@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class Welcome {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() { 
    console.log('ionViewDidLoad Welcome');
  }
  registrarse(){
    this.navCtrl.push(Register1);
  }
  iniciar(){
    this.navCtrl.push(Login);
  }

}

