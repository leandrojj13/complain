import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Introduccion } from '../introduccion/introduccion';
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';
import { AuthService } from '../../providers/auth-service';
import { Push, PushToken } from '@ionic/cloud-angular';
import { ENV } from '@app/config';

@Component({
  selector: 'login',   
  templateUrl: 'login.html',
})
export class Login {
  username: string;
  password: string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public authService: AuthService,
              private alertCtrl: AlertController, 
              public push: Push) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Register1');
  }

  entrar(){    
    this.authService.user.email = this.username;
    this.authService.user.password = this.password;
    this.authService.login((data)=>{
       if(ENV.IS_PRODUCTION){
            this.push.rx.notification()
            .subscribe((msg) => {
              alert(msg.title + ': ' + msg.text);
            });

            this.push.register().then((t: PushToken) => {
              return this.push.saveToken(t);
            }).then((t: PushToken) => {
              console.log('Token saved:', t.token);
            });
        }
       this.navCtrl.setRoot(TabsPage);
    },
    (response)=>{
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: response.message,
        buttons: ['Ok']
      });
      alert.present();
    })
  }
}
