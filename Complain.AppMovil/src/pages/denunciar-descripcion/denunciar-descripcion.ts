import { Component, ViewChild } from '@angular/core';
import { NavController, AlertController, ModalController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { List , reorderArray,LoadingController} from 'ionic-angular';
import { HomePage } from '../home/home';
import { DenunciarFinal } from '../denunciar-final/denunciar-final';
import { DataSaver } from '../../providers/data-saver';
import { StatusComplaint } from '../../models/enums/StatusComplaint';
import { Attached } from '../../models/Attached';
import { Complaint } from '../../models/ComplaintModel';
import { FileType } from '../../models/enums/FileType';
import { AlbumPics } from '../albumPics/albumPics';
import { AlbumVideos } from '../albumVideos/albumVideos';
import { AlbumAudios } from '../albumAudios/albumAudios';
import { File } from '@ionic-native/file';

import { MediaCapture } from '@ionic-native/media-capture';
import { FileChooser } from '@ionic-native/file-chooser';
import { AndroidPermissions } from '@ionic-native/android-permissions';


@Component({
  selector: 'denunciar-descripcion',
  templateUrl: 'denunciar-descripcion.html'
})

export class DenunciarDescripcion {
    
    @ViewChild("List") list:List;

    public listBase64Image: Array<Attached> = [];
    public listVideo: Array<Attached> = [];
    public listAudio: Array<Attached> = [];
 
    public descripcion : string;

    constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public androidPermissions: AndroidPermissions, public fileChooser: FileChooser, public camera : Camera, public mediaCapture: MediaCapture, public alertCtrl : AlertController, public dataSaver : DataSaver, private modalCtrl: ModalController, public _file: File) {
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, 
          this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
          this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION
      ]);
    }

    ngOnInit() {
      //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
      //Add 'implements OnInit' to the class.
      var file = this.dataSaver.file;
      var audio = this.dataSaver.audio;
      if(file){
        this.listBase64Image.push(file);
        file = null;
      }
      if(audio){
        this.listAudio.push(audio);
        audio = null;
      }
      this.listVideo = [];
    }
    
    presentConfirm() {
      
      let alert = this.alertCtrl.create({
        title: 'Estado',
        message: '¿Quieres enviar tu denuncia o guardarla para seguir más tarde?',
        buttons: [
          {
            text: 'Descartar',
            handler: () => {
                 this.descartarConfirm();
            }
          },
          {
            text: 'Guardar',
            role: 'guardar',
            handler: () => {
                this.dataSaver.complaint.description = this.descripcion;
                this.dataSaver.complaint.status = StatusComplaint.Saved;
                 this.dataSaver.save((res)=>{
                    this.navCtrl.popToRoot();
                });
            }
          },
          {
            text: 'Enviar',
            handler: () => {

                this.dataSaver.complaint.description = this.descripcion;
                this.dataSaver.complaint.status = StatusComplaint.Send;
                this.dataSaver.save((res)=>{
                    console.log(res);
                    var model = res.entity as Complaint;
                    window.localStorage.setItem("LASTID", model.codigo);
                    this.navCtrl.push(DenunciarFinal);
                }, (response)=>{
                    let alert = this.alertCtrl.create({
                      title: 'Error',
                      subTitle: response.message,
                      buttons: ['Ok']
                    });
                    alert.present();
                });
                
            }
          }
        ]
      });
      alert.present();
    }

     descartarConfirm(){
      let alert= this.alertCtrl.create({
        title: 'Confirmación',
        message: '¿Seguro que deseas descartar todos los pasos anteriores?, de ser así toda la información que has completado se perderá.',
        buttons: [
          {
            text: 'No',
            handler: () => {
              
            }
          },
          {
            text: 'Sí',
            handler: () => {
                this.navCtrl.popToRoot();
            }
          },
          
        ]
      });
      alert.present();
    }

    takePic(){
     const options : CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        mediaType: this.camera.MediaType.ALLMEDIA,
        targetHeight: 1000,
        targetWidth: 1000,
      }
  
      this.camera.getPicture(options).then(imgData =>{
      
        var file = new Attached();
            file.name = `Foto  ${this.dataSaver.listBase64Image.length + 1}`;
            file.data = `${imgData}`;
            file.url = null;
            file.FileType = FileType.Image;
            
        this.listBase64Image.push(file);
        this.dataSaver.listBase64Image = this.listBase64Image;
      }, err =>{});
   
    }
    

    takeVoice(){
      let loading = this.loadingCtrl.create({
        content: "Cargando..." 
      });

      var file = new Attached();
        file.name = `Audio ${this.dataSaver.listAudio.length + 1}`;

      this.mediaCapture.captureAudio().then(data =>{
        file.name = data[0].name;
        file.FileType = FileType.Audio;
        file.url = data[0].fullPath;
        const nameSplit = data[0].fullPath.split("/");

        const name = nameSplit[nameSplit.length - 1 ];
        const path = data[0].fullPath.replace(name, '');
      

        loading.present();
         
        return  this._file.readAsBinaryString(path, data[0].name);
  
      }).then(stringBinary =>{
           loading.dismiss();
            file.data = stringBinary;
            this.listAudio.push(file);
            this.dataSaver.listAudio = this.listAudio;
      });
    }

    recordVideo(){
      let loading = this.loadingCtrl.create({
        content: "Cargando..." 
      });
      
      var file = new Attached();
          file.name = `Video ${this.dataSaver.listVideo.length + 1}`;

      this.mediaCapture.captureVideo().then(data =>{
          file.name= data[0].name;
          file.FileType = FileType.Video;
          file.url = data[0].fullPath;
          const nameSplit = data[0].fullPath.split("/");

          const name = nameSplit[nameSplit.length - 1 ]
          const path = data[0].fullPath.replace(name, '');
          
          loading.present();
         
        return  this._file.readAsBinaryString(path, name);
      }).then(binaryString =>{
          file.data = binaryString;
          this.listVideo.push(file);
          this.dataSaver.listVideo = this.listVideo;
          loading.dismiss();
      });

    }

    chooseFile(){
      let options = {
          destinationType: this.camera.DestinationType.DATA_URL,
          sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
      };
      this.camera.getPicture(options).then(imgData =>{
        var file = new Attached();
            file.name = `Foto  ${this.dataSaver.listBase64Image.length + 1}`;
            file.data = `data:image/jpeg;base64,${imgData}`;
            file.url = null;
            file.FileType = FileType.Image;
            
        this.listBase64Image.push(file);
        this.dataSaver.listBase64Image = this.listBase64Image;
      }, err =>{});
    }
 
    viewAlbumPics(){
        let modal = this.modalCtrl.create(AlbumPics);
        modal.present();   
    }
    
    viewAlbumVideos(){
        let modal = this.modalCtrl.create(AlbumVideos);
        modal.present();   
    }

     viewAlbumAudios(){
        let modal = this.modalCtrl.create(AlbumAudios);
        modal.present();   
    }

}

