import { Component} from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';

@Component({
  selector: 'denunciar-final',
  templateUrl: 'denunciar-final.html'
})

export class DenunciarFinal {
    
    id : string;
 
    constructor(public navCtrl: NavController) {
        this.id = window.localStorage.getItem("LASTID");
    }
    inicio(){
        this.navCtrl.popToRoot();
    }
}

