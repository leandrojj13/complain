import { Component } from '@angular/core';
import { NavController, AlertController, NavParams, ViewController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { Welcome } from '../welcome/welcome';

@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {
  email: string;
  givenName: string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public authService: AuthService,
              private alertCtrl: AlertController,
              private viewCtrl: ViewController) {

    this.GetUserInfo();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
  }

  GetUserInfo(){    
    this.authService.GetUserInfo((response)=>{
       this.givenName = response.entity.givenName;
       this.email = response.entity.email;
    },
    (response)=>{
      
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: response.message,
        buttons: ['Ok']
      });
      alert.present();
    })
  }

  logout(){
     this.authService.removeToken();
     this.navCtrl.setRoot(Welcome);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
