import { Component } from '@angular/core';
import { NavController, ViewController  } from 'ionic-angular';
import { Complaint } from '../../models/ComplaintModel';
import { DataSaver } from '../../providers/data-saver';
import * as $  from 'jquery';
import { Attached } from '../../models/Attached';

@Component({
  selector: 'albumPics',
  templateUrl: 'albumPics.html'
})

export class AlbumPics {
  
  public listBase64Image: Array<Attached> = [];

  constructor(public navCtrl: NavController, private dataSaver : DataSaver, private viewCtrl: ViewController) {
    this.listBase64Image = this.dataSaver.listBase64Image;
  }

  ngOnInit() {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
   // $("#buttonLocation").click();
  }

   deleteImage(item){
      let index = this.dataSaver.listBase64Image.indexOf(item);
      if(index > -1){
          this.dataSaver.listBase64Image.splice(index, 1);
      }
    }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
