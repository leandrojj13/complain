import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MapDirective } from '../../components/map/map';
import { DenunciarDireccionWrite } from '../denunciar-direccion-write/denunciar-direccion-write';
import { DenunciarDescripcion } from '../denunciar-descripcion/denunciar-descripcion';
import { DataSaver } from '../../providers/data-saver';
import {AddressType} from "../../models/enums/AddressType";

import * as $  from 'jquery';

@Component({
  selector: 'denunciar-direccion',
  templateUrl: 'denunciar-direccion.html'
})

export class DenunciarDireccion {
    public isPickupRequest: boolean;
    public objectDirection : any;
    constructor(public navCtrl: NavController, public dataSaver : DataSaver) {
      this.isPickupRequest = false;
    }
    ngOnInit() {
      //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
      //Add 'implements OnInit' to the class.
    }

  ionViewDidLoad() {
    $(document).ready(function(){
     setTimeout( ()=>{
        $("#centerLocation").click();
        console.log("ready");
     },1000);
    });
  }
    writeAddress(){
        this.navCtrl.push(DenunciarDireccionWrite);
    }

    confimAddress(){
    //  this.isPickupRequest = true;
      this.dataSaver.complaint.longitude = this.objectDirection.lng();
      this.dataSaver.complaint.latitude = this.objectDirection.lat();
      this.dataSaver.complaint.addressType = AddressType.GPS;
      this.navCtrl.push(DenunciarDescripcion);
    }

    updateDirection(data){
      this.objectDirection = data;
    }

    cancelAddress(){
      //this.isPickupRequest = false;
    }

}
