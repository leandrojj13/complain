import { Component } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { NavController, ViewController  } from 'ionic-angular';
import { Complaint } from '../../models/ComplaintModel';
import { DataSaver } from '../../providers/data-saver';
import * as $  from 'jquery';
import { Attached } from '../../models/Attached';
import { VideoPlayer } from '@ionic-native/video-player';

@Component({
  selector: 'albumVideos',
  templateUrl: 'albumVideos.html'
})

export class AlbumVideos {
  
  public listVideo: Array<Attached> = [];

  constructor(public navCtrl: NavController, public videoPlayer: VideoPlayer, public androidPermissions: AndroidPermissions, private dataSaver : DataSaver, private viewCtrl: ViewController) {
    this.listVideo = this.dataSaver.listVideo;
  
  }

   deleteVideo(item){
      let index = this.dataSaver.listVideo.indexOf(item);
      if(index > -1){
          this.dataSaver.listVideo.splice(index, 1);
      }
    }


  ngOnInit() {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
   // $("#buttonLocation").click();

   
  }


  dismiss() {
    this.viewCtrl.dismiss();
  }
}
