import { Component, ViewChild, ElementRef } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NavController } from 'ionic-angular';
import { MediaCapture } from '@ionic-native/media-capture';
import { DenunciarTipo } from '../denunciar-tipo/denunciar-tipo';
import { AlertController } from 'ionic-angular';
import { VideoPlayer } from '@ionic-native/video-player';

@Component({
  selector: 'camera-page',
  templateUrl: 'camera.html'
})

export class CameraPage {

  @ViewChild('myVideo') myVideo : ElementRef;
   public  videoSRC = "";

  public base64Image = "";
  constructor(public navCtrl: NavController, public camera : Camera, public mediaCapture: MediaCapture, private alertCtrl: AlertController) {

  }

  ngOnInit() {
    this.takePic();
  }

  takePic(){
    const options : CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      mediaType: this.camera.MediaType.ALLMEDIA,
      targetHeight: 1000,
      targetWidth: 1000,
    }
 
    this.camera.getPicture(options).then(imgData =>{
        this.base64Image = 'data:image/jpeg;base64,' + imgData;
    }, err =>{

    });
    
  }
  
  startRecording(){
    this.mediaCapture.captureVideo().then(data =>{
        this.videoSRC = data[0].fullPath;
        this.seguir();
    });
  }

  eliminar(){
    this.base64Image = "";
    this.videoSRC = "";
  }

  seguir(){
     this.navCtrl.push(DenunciarTipo);
  }

}
