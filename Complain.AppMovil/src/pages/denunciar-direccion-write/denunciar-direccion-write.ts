import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { DenunciarDescripcion } from '../denunciar-descripcion/denunciar-descripcion';
import { DataSaver } from '../../providers/data-saver';
import {AddressType} from "../../models/enums/AddressType";

@Component({
  selector: 'denunciar-direccion-write',
  templateUrl: 'denunciar-direccion-write.html'
})

export class DenunciarDireccionWrite {
    
    public calle;
    public residencia;
    public sector;
    public descripcion;

    constructor(public navCtrl: NavController,  private dataSaver : DataSaver,  private alertCtrl: AlertController) {
   
    }

    goDenunciarPage(){

        if(!this.calle){
             let alert = this.alertCtrl.create({
                title: 'Validación',
                subTitle: "Debería especificar la calle.",
                buttons: ['Ok']
            });
            alert.present();
        }
        else if(!this.residencia){
            let alert = this.alertCtrl.create({
                title: 'Validación',
                subTitle: "Debería especificar la residencia.",
                buttons: ['Ok']
            });
            alert.present();

        }
         else if(!this.sector){
            let alert = this.alertCtrl.create({
                title: 'Validación',
                subTitle: "Debería especificar el sector.",
                buttons: ['Ok']
            });
            alert.present();

        }else{
            this.dataSaver.complaint.addressType = AddressType.Location;
            this.dataSaver.complaint.street = this.calle;
            this.dataSaver.complaint.residencia = this.residencia;
            this.dataSaver.complaint.sector = this.sector;
            this.dataSaver.complaint.addressDescription = this.descripcion;
            this.navCtrl.push(DenunciarDescripcion);
        }
        
    }

}
