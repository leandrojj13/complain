import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { DenunciarTipo } from '../denunciar-tipo/denunciar-tipo';
import { DenunciarDireccion } from '../denunciar-direccion/denunciar-direccion';
import { Historial } from '../historial/historial';
import { DataSaver } from '../../providers/data-saver';
import { ComplaintType } from '../../models/ComplaintTypeModel';
import { PerfilPage } from '../perfil/perfil';

import * as $  from 'jquery';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    
  _navCtrl: NavController;
  complaintTypes: Array<ComplaintType>;

  constructor(public navCtrl: NavController, 
              private dataSaver : DataSaver, 
              public modalCtrl: ModalController) { 

    this._navCtrl = navCtrl;
   
  }

    viewPerfil() {
        let modal = this.modalCtrl.create(PerfilPage);
        modal.present();
    }

  ionViewDidLoad() {
   this.initData();
  }
  
  ngOnInit() {
  
  }
  
  historial(){
      this.dataSaver.getComplaintServer();
      this.dataSaver.getMessagesServer();
      this.navCtrl.push(Historial);
  }

  initData(){
    this.dataSaver.getComplaintTypesServer(()=>{
        var allComplainTypes = this.dataSaver.complaintTypes;
        console.log(allComplainTypes);
        for (var x = 0; x < allComplainTypes.length; x++) {
            var element = allComplainTypes[x];
            if(element.name.toLowerCase() == "otros"){
                allComplainTypes.splice(x , 1);
                break;
            }
        }
        this.complaintTypes = allComplainTypes.slice(0, 7);
        this.initCircle();
    });
  }
  
  initCircle(){
        var angleStart = -360;
      // jquery rotate animation
      function rotate(li,d) {
          $({d:angleStart}).animate({d:d}, {
              step: function(now) {
                  $(li)
                    .css({ transform: 'rotate('+now+'deg)' })
                    .find('label')
                        .css({ transform: 'rotate('+(-now)+'deg)' });
              }, duration: 0
          });
      }

      // show / hide the options
      function toggleOptions(s) {
          $(s).toggleClass('open');
          var li = $(s).find('li');
          var deg =  360/li.length;
            for(var i=0; i<li.length; i++) {
              var d = i*deg;
              $(s).hasClass('open') ? rotate(li[i],d) : rotate(li[i],angleStart);
          }
      }

    $('.selector button').click(function(e) {
        toggleOptions($(this).parent());
    });

     setTimeout(function() { toggleOptions('.selector'); }, 500);
   }

   tipoElegido(tipo : ComplaintType){
        this.dataSaver.complaint.complaintTypeId = tipo.id;
        this._navCtrl.push(DenunciarDireccion);
   }
   
   goToDenunciarTipo() {
        this._navCtrl.push(DenunciarTipo);
    }

}
