import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DenunciarDireccion } from '../denunciar-direccion/denunciar-direccion';
import { DataSaver } from '../../providers/data-saver';
import { ComplaintType } from '../../models/ComplaintTypeModel';


import * as $  from 'jquery';

@Component({
  selector: 'denunciar-tipo',
  templateUrl: 'denunciar-tipo.html'
})

export class DenunciarTipo {

   _navCtrl: NavController;
   complaintTypes: Array<ComplaintType>;

  constructor(public navCtrl: NavController,  private dataSaver : DataSaver) {
        this._navCtrl = navCtrl;
  }
   ngOnInit(){
      this.initData();
      // show / hide the options
      function toggleOptions(s) {
          $(s).toggleClass('open');
      }
      setTimeout(function() { toggleOptions('denunciar-tipo .selector'); }, 100);

   }


    initData(){
        var allComplainTypes =  this.dataSaver.getComplaintTypes() as Array<ComplaintType>;
        this.complaintTypes = allComplainTypes;
    }

   goToDenunciarDireccion(complaintType: ComplaintType) {
        this.dataSaver.complaint.complaintTypeId = complaintType.id;
        this._navCtrl.push(DenunciarDireccion);
    }
}
