import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NavController, NavParams, AlertController  } from 'ionic-angular';
import { Register2 } from '../register-2/register-2';
import { AuthService } from '../../providers/auth-service';

@Component({
  selector: 'page-register-1',
  templateUrl: 'register-1.html',
})

export class Register1 {
  form: FormGroup;
  submitAttempt: boolean;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public authService: AuthService, 
              private alertCtrl: AlertController ) {
                
    this.form = new FormGroup(
      {
        name: new FormControl('', Validators.required)
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Register1');
  }
  next(){
    this.submitAttempt = true;

    if(this.form.valid){
      this.authService.user.name = this.form.value.name;
      this.navCtrl.push(Register2);    
    }
  }
}
