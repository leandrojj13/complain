import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { Introduccion } from '../introduccion/introduccion';
import { AuthService } from '../../providers/auth-service';
import { Response }  from '../../models/ResponseModel';

@Component({
  selector: 'page-register-3',
  templateUrl: 'register-3.html',
})
export class Register3 {
  form: FormGroup;
  submitAttempt: boolean;
  terms: boolean;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public authService: AuthService, 
              private alertCtrl: AlertController) {

  this.form = new FormGroup(
      {
        password: new FormControl('', Validators.compose([Validators.required, Validators.minLength(5)])),
        confirmPassword: new FormControl()
      }, 
      (g: FormGroup) => {
        return g.get('password').value === g.get('confirmPassword').value
            ? null : {'mismatch': true};
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Register1');
  }
  next(){
    this.submitAttempt = true;

    if(this.form.valid){

      if(this.terms){
        this.authService.user.password = this.form.value.password;
        this.authService.registerAccount((response: Response<any>)=>{
          this.navCtrl.push(Introduccion);    
        },
        (response)=>{
          let alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: response.message,
            buttons: ['Ok']
          });
          alert.present();
        });
      }
      else{
        let alert = this.alertCtrl.create({
          title: 'Aviso',
          subTitle: "Debe de aceptar los términos y condiciones para continuar.",
          buttons: ['Ok']
        });
        alert.present();
      }

    }
  }
}
