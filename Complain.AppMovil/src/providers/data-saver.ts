import { Injectable } from '@angular/core';
import { Http,  Headers, RequestOptions } from '@angular/http';
import { ENV } from '@app/config';
import { LoadingController, AlertController} from 'ionic-angular';
import { Attached } from '../models/Attached';
import { BroadCastModel }  from '../models/BroadCastModel';
import { Complaint }  from '../models/ComplaintModel';
import { Response }   from '../models/ResponseModel';
import { StatusComplaint } from "../models/enums/StatusComplaint";
import { ComplaintType } from '../models/ComplaintTypeModel';
import * as $  from 'jquery';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

/*
  Generated class for the DataSaver provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class DataSaver {
 
  complaint: Complaint = {} as Complaint;
  complaintTypes : Array<ComplaintType> = [];
  broadCasts : Array<BroadCastModel> = [];
  APIURL : String = ENV.APIURL;
  file: Attached;
  audio: Attached;
  detailView : Complaint;

  listBase64Image: Array<Attached> = [];
  listVideo: Array<Attached> = [];
  listAudio: Array<Attached> = [];

  constructor(private http: Http, private loadingCtrl: LoadingController, public alertCtrl : AlertController) {
    this.init();
  }

  private handleError(error: any) {
      console.error('An error occurred', error); // for demo purposes only
      return Promise.reject(error.message || error);
  }

  init() {
    this.getComplaintServer();  
    //this.getComplaintTypesServer();  
  }

  getComplaintTypes(){
     var localData = window.localStorage['complainttype'] || "";
     if(localData){
       var data = JSON.parse(localData);
       for(let i = 0; i < data.entities.length; i++){
          this.complaintTypes.push(data.entities[i]);
       }
       return data.entities;
     }
     return [];
  }

  getComplaint(){
     var localData = window.localStorage['complaint'] || "";
     if(localData){
       var data = JSON.parse(localData);
       return data.entities;
     }
     return [];
  }
  
  getComplaintServer(){
    let loading = this.loadingCtrl.create({
      content:  "Cargando..." 
    });

    var getInfo = ()=>{ 
      this.http.get(`${this.APIURL}complaint`).subscribe((res : any) => {
        if(res._body){
            window.localStorage.setItem('complaint', res._body);
            this.getComplaintTypes();
        }
        loading.dismiss();
        
      });
     }

    if(!window.localStorage['complaint']){
      loading.present().then(()=>{
        getInfo();
      });
    }else{
      getInfo();
    }

    
  }

  getComplaintTypesServer(callback?){
    
    let loading = this.loadingCtrl.create({
      content:  "Cargando..." 
    });

    var getInfo = ()=>{ 
       this.http.get(`${this.APIURL}complainttype`).subscribe((res : any) => {
          if(res._body){
              window.localStorage.setItem('complainttype', res._body);
              if(callback){
                  this.getComplaintTypes();
                  callback();
              }
          }
          loading.dismiss();
        });
     }

    if(!window.localStorage['complainttype']){
      loading.present().then(()=>{
        getInfo();
      });
    }else{
      getInfo();
    }
  
  }

  
  getMessagesServer(callback?){
    
    let loading = this.loadingCtrl.create({
      content:  "Cargando..." 
    });

     if(window.localStorage['messages']){
        this.broadCasts = JSON.parse(window.localStorage['messages']).entities;
     }


    var getInfo = ()=>{ 
       this.http.get(`${this.APIURL}broadCast`).subscribe((res : any) => {
          if(res._body){
              window.localStorage.setItem('messages', res._body);
              this.broadCasts = JSON.parse(res._body);
              
              if(callback){
                  callback();
              }
          }
          loading.dismiss();
        });
     }

    if(!window.localStorage['messages']){
      loading.present().then(()=>{
        getInfo();
      });
    }else{
      getInfo();
    }
  
  }


  save(callback: Function, errorCallback?: Function){
    // Aquí se enviará toda la data recaudadad en el objecto complaint
    // Tambien se enviará los archivos adjuntos de la denuncia tales como pic and note voice.
  
    let loading = this.loadingCtrl.create({
      content: this.complaint.status == StatusComplaint.Saved ? "Guardando..." : "Enviando..." 
    });

    loading.present().then(()=>{
      this.complaint.attachments = [];
      this.complaint.userId = localStorage.getItem('user-id');
      $.merge(this.complaint.attachments, this.listAudio);
      $.merge(this.complaint.attachments, this.listBase64Image);
      $.merge(this.complaint.attachments, this.listVideo);

      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      this.http
          .post(`${this.APIURL}Complaint`, JSON.stringify(this.complaint),options)
          .toPromise()
          .then(httpResult =>{
            loading.dismiss();
            const response = httpResult.json() as Response<any>;
            if(response.success){
              callback(response);
              this.complaint = {} as Complaint;
            }
          })        
          .catch((httpResult)=>{
            if (errorCallback) {
                loading.dismiss();
                const response = httpResult.json() as Response<any>;
                errorCallback(response);
            }
        });;
    });

    
  }
}
