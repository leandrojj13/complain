import { Injectable } from '@angular/core';
import { Http,  Headers, RequestOptions } from '@angular/http';
import { ENV } from '@app/config'
import { LoadingController} from 'ionic-angular';
import { Response }   from '../models/ResponseModel';
import { User }   from '../models/UserModel';
import { AuthToken }   from '../models/AuthTokenModel';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AuthService  {
 
  API : String = ENV.API;
  user: User = {id: null, name: null, password: null, email: null};
  defaultHeader: Headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private http: Http, private loadingCtrl: LoadingController) {

  }

    private storeToken(tokens: AuthToken): void {
        localStorage.setItem('auth-tokens', JSON.stringify(tokens));
        localStorage.setItem('user-email', JSON.stringify(this.user.email));
    }

    private retrieveTokens(): AuthToken {
        const tokensString = localStorage.getItem('auth-tokens');
        const tokensModel: AuthToken = tokensString == null ? null : JSON.parse(tokensString);
        return tokensModel;
    }

    private retrieveUserEmail(): string {
        const userEmailString = localStorage.getItem('user-email');
        const useremail: string = userEmailString == null ? null : JSON.parse(userEmailString);
        return useremail;
    }

    public removeToken(): void {
        localStorage.removeItem('auth-tokens');
        localStorage.removeItem('user-email');
    }
    
    public isAuth(): boolean {
        const tokensString = localStorage.getItem('auth-tokens');
        const tokensModel: boolean = tokensString == null ? false : true;
        return tokensModel;
    }
//   private handleError(error: any) {
//       console.error('An error occurred', error); // for demo purposes only
//       return Promise.reject(error.message || error);
//   }
  GetUserInfo(callback, errorcalback?: Function){

    let loading = this.loadingCtrl.create({
      content:  "Cargando información..." 
    });
    loading.present();

    var userEmail = this.retrieveUserEmail();
    let options = new RequestOptions({ headers: this.defaultHeader });
    this.http
        .get(`${this.API}account/UserInfo?email=${userEmail}`, options)
        .toPromise()
        .then(httpResult => {
            if (httpResult.ok) {
                const response = httpResult.json() as Response<any>;
                if (response.success) {
                    loading.dismiss();
                    callback(response);
                }
            }
        })
        .catch((httpResult)=>{
            if (errorcalback) {
                loading.dismiss();
                const response = httpResult.json() as Response<any>;
                errorcalback(response);
            }
        });
  } 

  CheckUserExist(callback, errorcalback?: Function){

    let loading = this.loadingCtrl.create({
      content:  "Verificando usuario..." 
    });
    loading.present();

    let options = new RequestOptions({ headers: this.defaultHeader });
    this.http
        .post(`${this.API}account/CheckUserExist`, JSON.stringify(this.user.email), options)
        .toPromise()
        .then(httpResult => {
            if (httpResult.ok) {
                const response = httpResult.json() as Response<any>;
                if (response.success) {
                    loading.dismiss();
                    callback(response);
                }
            }
        })
        .catch((httpResult)=>{
            if (errorcalback) {
                loading.dismiss();
                const response = httpResult.json() as Response<any>;
                errorcalback(response);
            }
        });
  } 

  registerAccount(callback, errorcalback?: Function){

    let loading = this.loadingCtrl.create({
      content:  "Registrando..." 
    });
    loading.present();

    let options = new RequestOptions({ headers: this.defaultHeader });
    this.http
        .post(`${this.API}account/register`, JSON.stringify(this.user), options)
        .toPromise()
        .then(httpResult => {
            if (httpResult.ok) {
                const response = httpResult.json() as Response<any>;
                if (response.success) {
                    loading.dismiss();
                    callback(response);
                }
            }
        })
        .catch((httpResult)=>{
            if (errorcalback) {
                loading.dismiss();
                const response = httpResult.json() as Response<any>;
                errorcalback(response);
            }
        });
  } 

  login(callback, errorcalback?: Function){
    
    let loading = this.loadingCtrl.create({
      content:  "Ingresando..." 
    });
    loading.present();

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded;'});
    let options = new RequestOptions({ headers: headers });

    const params = new URLSearchParams();
    params.append("grant_type", "password"); 
    params.append("username", this.user.email); 
    params.append("password", this.user.password); 
        
    this.http
        .post(`${this.API}connect/token`, params.toString(), options)
        .toPromise()
        .then(httpResult => {
            if (httpResult.ok) {
                const response = httpResult.json() as AuthToken;
                loading.dismiss();
                this.storeToken(response);
                callback(response);

                this.GetUserInfo((response)=>{
                    this.user.id = response.entity.id;
                    localStorage.setItem('user-id', this.user.id);
                },
                (response)=>{
               
                })
            }
        })
        .catch((httpResult)=>{
            if (errorcalback) {
                loading.dismiss();
                const response = httpResult.json() as Response<any>;
                errorcalback(response);
            }
        });
  }
}
