import { Geolocation } from '@ionic-native/geolocation';
import { LoadingController} from 'ionic-angular';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {googlemaps} from 'googlemaps'; 
import {Observable} from 'rxjs/Observable'; 
import {PickupDirective} from '../pickup/pickup'; 

@Component({
  selector: 'map',
  templateUrl: 'map.html'
})
export class MapDirective implements OnInit{
 
  @Input()  isPickupRequest: boolean;
  @Output() OnPickUpChange : EventEmitter<any> = new EventEmitter();

  public map : google.maps.Map;
  public isMapIdle: boolean;

  constructor(public Geolocation : Geolocation, public loadingCtrl: LoadingController) {
  }

  ngOnInit() {
    this.map = this.createMap();  
   // this.centerLocation(null);
    this.addMapEventListener();
  }

  createMap(location = new google.maps.LatLng(18.5048961,-69.8602816)){
      let mapOptions = {
          center: location, 
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true
      }
      let mapElement = document.getElementById('map');
      let map  = new google.maps.Map(mapElement, mapOptions); 

      return map;
  }
  
  getCurrentLocation(){

    let loading = this.loadingCtrl.create({
      content: "Cargando..."
    });

    loading.present();

    let options = {timeout: 1000, enableHighAccuracy: true};

    let locationObs = Observable.create(observable=>{
        this.Geolocation.getCurrentPosition(options).then(resp =>{
              let lat = resp.coords.latitude;
              let lng = resp.coords.longitude;

              let location = new google.maps.LatLng(lat, lng);
                observable.next(location);
                 loading.dismiss();
          },
            (error)=>{
              console.log("Geolocation err:" + error);
               loading.dismiss();
            }
          );  
    });

    return locationObs;
  }

  addMapEventListener(){
   
    google.maps.event.addListener(this.map, "dragstart",()=>{
      this.isMapIdle = false;
      this.verificarPlace();
    });

    google.maps.event.addListener(this.map, "idle",()=>{
      this.isMapIdle = true;
      this.verificarPlace();
          
    });

    google.maps.event.addListener(this.map, 'dragend', ()=> {
      this.verificarPlace();
    });

  }

  verificarPlace(){
     var strictBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(18.5107999, -69.8709472),
      new google.maps.LatLng(18.505583,  -69.8001762)
    );

     if (strictBounds.contains(this.map.getCenter()))
     { return;
     }
     // We're out of bounds - Move the map back within the bounds

     var c = this.map.getCenter(),
         x = c.lng(),
         y = c.lat(),
         maxX = strictBounds.getNorthEast().lng(),
         maxY = strictBounds.getNorthEast().lat(),
         minX = strictBounds.getSouthWest().lng(),
         minY = strictBounds.getSouthWest().lat();

     if (x < minX) x = minX;
     if (x > maxX) x = maxX;
     if (y < minY) y = minY;
     if (y > maxY) y = maxY;

     this.map.setCenter(new google.maps.LatLng(y, x));
  }
  updatePickUp(data){
   this.OnPickUpChange.emit(data);
  }

  centerLocation(location){
    var self = this;
    
    if(location){
      this.map.panTo(location)
    }else{
        
        this.getCurrentLocation().subscribe((location)=>{
          this.isMapIdle = false;

          this.map.panTo(location);
          setTimeout(()=>{
            self.isMapIdle = true;
          }, 600);

        });
    }
  }

  setLocation(lat, lng){
    let location = new google.maps.LatLng(parseInt(lat), parseInt(lng));
    this.map.panTo(location);
  }
}
