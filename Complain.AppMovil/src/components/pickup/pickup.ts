import { Component, Input, OnChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'pickup',
  templateUrl: 'pickup.html',
})

export class PickupDirective implements OnChanges{
  @Input() isPinSet :boolean;
  @Input() map : google.maps.Map;
  @Output() OnChange : EventEmitter<any> = new EventEmitter();

  private pickupMarker: google.maps.Marker;
  private popup: google.maps.InfoWindow;

  constructor() {
 
  }

  ngOnChanges(changes) {
    if(this.isPinSet){
        this.showPickUpMarker();
    }else{
        this.removePickUpMarker();
    }
  }

  showPickUpMarker(){
    this.pickupMarker = new google.maps.Marker({
      map: this.map,
//      animation: google.maps.Animation.DROP,
      position: this.map.getCenter(),
      icon: "assets/img/pinmap.png"
    });
    this.OnChange.emit(this.pickupMarker.getPosition());
  //  this.pickupMarker.getPosition();
    //this.showPickUpTime();
  }

  removePickUpMarker(){
      if(this.pickupMarker){
          this.pickupMarker.setMap(null);
      }
  }

  showPickUpTime(){
    this.popup = new google.maps.InfoWindow({
      content: "<h5>Localización de la denuncia</h5>"
    });

    this.popup.open(this.map, this.pickupMarker);

    google.maps.event.addListener(this.pickupMarker, 'click', ()=>{
      this.popup.open(this.map, this.pickupMarker);
    });

  }

}
