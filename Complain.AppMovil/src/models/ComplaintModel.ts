import {StatusComplaint} from "../models/enums/StatusComplaint";
import {AddressType} from "../models/enums/AddressType";
import { Attached } from '../models/Attached';

export interface Complaint{
    id: string;
    codigo: string;
    description: string;
    status: StatusComplaint;
    complaintTypeName: string;
    sector: string;
    street: string;
    addressDescription: string;
    residencia: string;
    longitude: string;
    latitude: string;
    addressType: AddressType;
    complaintTypeId : string;
    userId: string;
    attachments : Array<Attached>;
}

