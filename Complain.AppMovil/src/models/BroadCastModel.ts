export interface BroadCastModel {
    id: string;
    title: string;
    message: string;
}