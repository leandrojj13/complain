export interface ComplaintType{
    id: string;
    codigo: string;
    name: string;
    description: string;
    imageUrl: string;
}

