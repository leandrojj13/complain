import {FileType} from "../models/enums/FileType";

export class Attached{
   public FileType  : FileType;
   public name : string;
   public data : string;
   public url : string;
}
