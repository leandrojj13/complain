import { Component } from '@angular/core';
import { Platform, Config} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Complaint } from "../models/ComplaintModel"
import { Welcome } from '../pages/welcome/welcome';
import { TabsPage } from '../pages/tabs/tabs';
import { AuthService } from '../providers/auth-service';
import { Push, PushToken } from '@ionic/cloud-angular';
import { ENV } from '@app/config';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {

 rootPage:any = undefined;
 //rootPage:any = TabsPage;
  
  constructor(platform: Platform, 
              statusBar: StatusBar, 
              splashScreen: SplashScreen, 
              config: Config, 
              authService: AuthService, 
              public push: Push) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
     

     

      if(authService.isAuth()){
        if(ENV.IS_PRODUCTION){
            this.push.rx.notification()
            .subscribe((msg) => {
              alert(msg.title + ': ' + msg.text);
            });

            this.push.register().then((t: PushToken) => {
              return this.push.saveToken(t);
            }).then((t: PushToken) => {
              console.log('Token saved:', t.token);
            });
        }
        this.rootPage = TabsPage;
      }
      else{
        this.rootPage = Welcome;
      }
      config.set('backButtonText', "Atrás");
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
