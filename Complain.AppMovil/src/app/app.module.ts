import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from "@angular/http";

import { IonicApp, 
        IonicModule, 
        IonicErrorHandler, 
        LoadingController, 
        NavController, 
        ViewController } from 'ionic-angular';

import {Push, PushToken, CloudSettings, CloudModule } from '@ionic/cloud-angular';

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '5b529215',
  },
  'push': {
    'sender_id': '577162711204',
    'pluginConfig': {
      'ios': {
        'badge': true,
        'sound': true
      },
      'android': {
        'iconColor': '#343434'
      }
    }
  }
};

import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MediaCapture } from '@ionic-native/media-capture';
import { FileChooser } from '@ionic-native/file-chooser';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { VideoPlayer } from '@ionic-native/video-player';


import { DataSaver } from '../providers/data-saver';
import { AuthService } from '../providers/auth-service';
import { MapDirective } from '../components/map/map';
import { PickupDirective } from '../components/pickup/pickup'; 
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media';
import { File } from '@ionic-native/file';

import { CameraPage } from '../pages/camera/camera';
import { MicrophonePage } from '../pages/microphone/microphone';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { DenunciarTipo } from '../pages/denunciar-tipo/denunciar-tipo';
import { DenunciarDireccion } from '../pages/denunciar-direccion/denunciar-direccion';
import { DenunciarDireccionWrite } from '../pages/denunciar-direccion-write/denunciar-direccion-write';
import { DenunciarDescripcion } from '../pages/denunciar-descripcion/denunciar-descripcion';
import { DenunciarFinal } from '../pages/denunciar-final/denunciar-final';
import { Welcome } from '../pages/welcome/welcome';
import { Register1 } from '../pages/register-1/register-1';
import { Register2 } from '../pages/register-2/register-2';
import { Register3 } from '../pages/register-3/register-3';
import { Introduccion } from '../pages/introduccion/introduccion';
import { Login } from '../pages/login/login';
import { Historial } from '../pages/historial/historial';
import { HistorialDetalle } from '../pages/historialDetalle/historialDetalle';
import { AlbumPics } from '../pages/albumPics/albumPics';
import { AlbumVideos } from '../pages/albumVideos/albumVideos';
import { AlbumAudios } from '../pages/albumAudios/albumAudios';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { PerfilPage } from '../pages/perfil/perfil';



@NgModule({
  declarations: [
    MyApp,
    CameraPage,
    MicrophonePage,
    HomePage,
    TabsPage,
    DenunciarTipo,
    DenunciarDireccion,
    MapDirective,
    PickupDirective,
    DenunciarDescripcion,
    DenunciarDireccionWrite,
    DenunciarFinal,
    Welcome,
    Register1,
    Register2,
    Register3,
    Introduccion,
    Login,
    Historial,
    HistorialDetalle,
    AlbumPics,
    AlbumVideos,
    AlbumAudios,
    PerfilPage,
    ForgotPasswordPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    CloudModule.forRoot(cloudSettings)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CameraPage,
    MicrophonePage,
    HomePage,
    TabsPage,
    DenunciarTipo,
    DenunciarDireccion,
    MapDirective,
    PickupDirective,
    DenunciarDescripcion,
    DenunciarDireccionWrite,
    DenunciarFinal,
    Welcome,
    Register1,
    Register2,
    Register3,
    Introduccion,
    Login,
    Historial,
    HistorialDetalle,
    AlbumPics,
    AlbumVideos,
    AlbumAudios,
    PerfilPage,
    ForgotPasswordPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    Camera,
    MediaCapture,
    DataSaver,
    FileChooser,
    AndroidPermissions,
    AuthService,
    VideoPlayer,
    StreamingMedia,
    File,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
