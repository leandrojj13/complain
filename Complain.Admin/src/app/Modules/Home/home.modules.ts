import { NgModule } from '@angular/core';
import { BrowserModule} from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";

import { HomeComponent } from "./Components/home.component";
import { BarChartComponent  } from "./Components/barchart.component";
import { LineChartComponent  } from "./Components/linechart.component";

import { ChartsModule  } from "../../Directives/chart/chartDirective";
import { MyDatePickerModule } from 'mydatepicker';


@NgModule({
  declarations: [ HomeComponent, BarChartComponent, LineChartComponent ],
  imports:[BrowserModule, FormsModule, ChartsModule, MyDatePickerModule],
  exports: [  ]
})
export class HomeModule {}