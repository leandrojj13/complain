import {Component} from '@angular/core';
import { BaseapiService } from "../../../Services/baseapi.service";
import { BarChartModel } from "../../../Models/BarChartModel";
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import * as moment from 'moment';

@Component({
    selector:'barchart-statistics',
    templateUrl: `../../Templates/barchart.template.html`
}) 

export class BarChartComponent{

    private actualDate: moment.Moment;;
    
    /** DatePicker */
    private myDatePickerOptions: IMyDpOptions;
    private localeDatePicker: string;
    private myDatePickerModel: any;
    /** End DatePicker */

    
    /** BarChart */
    public barChartColors =  [
        {  
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "#18bc9c",
            hoverBackgroundColor: "rgba(24, 188, 156, 0.46)",
            hoverBorderColor: "#18bc9c"
        }
    ];
    public barChartLabels:string[] = [];
    public barChartData:any = [];
    public barChartType:string = 'horizontalBar';
    public barChartLegend:boolean = true;
    public barChartDatasets:any[] = [
        {
            label: " Denuncias",
            borderWidth: 2
        }
    ];
    public barChartOptions:any = {
        scaleShowVerticalLines: false,
        responsive: true,
        scales: {
            xAxes: [{
                ticks: {
                    min: 0,
                    suggestedMax: 10,
                    stepSize: 1
                }
            }]
        }
    };
    /** End BarChart */
    
    /**
     * Controls
     */
    public filterTypes: Array<any>;
    public selectedFilter: string;
    /**
     * Controls
     */
    constructor(private baseApi: BaseapiService<BarChartModel>){
       moment.locale('es');
       this.actualDate = moment();

       this.myDatePickerOptions = {
            // other options...
            dateFormat: 'dd.mm.yyyy',
            inline: true
        };
        this.localeDatePicker = "es";
        this.myDatePickerModel =  { date: { 
                                year: this.actualDate.year(), 
                                month: this.actualDate.month() + 1, 
                                day: this.actualDate.date() 
                            } 
                        };

        this.filterTypes = new Array("Fecha", "Todos");
        this.selectedFilter = "Fecha";
    }

    ngOnInit() {
        this.getStatistic(this.actualDate.toDate(), (response: Array<BarChartModel>) => {
            var complaintTypesNames = response.map(x=> x.name);
            var data = response.map(x=> x.count);
            this.barChartLabels = complaintTypesNames;
            this.barChartData = data;
        });
    }

    // events
    public chartClicked(e:any):void {
        console.log(e);
    }
    
    public chartHovered(e:any):void {
        console.log(e);
    }

    public selectedFilterChange(value: string){
        if(value == "Todos"){
            this.getStatistic(null, (response: Array<BarChartModel>) => {
                var data = response.map(x=> x.count);
                this.barChartData = data
            });
        }else{
            var date = new Date(this.myDatePickerModel.date.year, this.myDatePickerModel.date.month-1, this.myDatePickerModel.date.day);
            this.actualDate = moment(date);
            this.getStatistic(date, (response: Array<BarChartModel>) => {
                var data = response.map(x=> x.count);
                this.barChartData = data
            });
        }
    }

    // dateChanged callback function called when the user select the date. This is mandatory callback
    // in this option. There are also optional inputFieldChanged and calendarViewChanged callbacks.
    public onDateChanged(event: IMyDateModel) {
        if(event.jsdate){
            this.actualDate = moment(event.jsdate);
            this.getStatistic(event.jsdate, (response: Array<BarChartModel>) => {
                var data = response.map(x=> x.count);
                this.barChartData = data
            });
        }
        // event properties are: event.date, event.jsdate, event.formatted and event.epoc
    }

    private getStatistic(filterDate: Date, callback: Function){
        this.baseApi.getAll(`Complaint/GetComplaintsMadeBarChart?filterDate=${filterDate ? filterDate.toUTCString() : filterDate}`, callback);
    }
}
