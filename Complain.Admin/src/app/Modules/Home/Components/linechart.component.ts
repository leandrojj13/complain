import {Component, SimpleChanges} from '@angular/core';
import { BaseapiService } from "../../../Services/baseapi.service";
import { ComplaintTypeModel } from "../../../Models/ComplaintTypeModel";

@Component({
    selector:'linechart-statistics',
    templateUrl: `../../Templates/linechart.template.html`
}) 

export class LineChartComponent{

    private actualDate: Date = new Date();
    private months: Array<string> = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

    /**
     * LineChart
     */
    public lineChartLabels:Array<string> = this.months;
    public lineChartData:Array<any> = [
        {data: new Array(this.lineChartLabels.length)},
    ];
    public lineChartOptions:any = {
        responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    min: 0,
                    suggestedMax: 20
                }
            }]
        }
    };
    public lineChartColors:Array<any> = [
        {
            label: " Denuncias",
            fill: true,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderWidth: 2,
            borderColor: "#18bc9c",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 5,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(24, 188, 156, 0.46)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            spanGaps: false,
        }
    ];
    public lineChartLegend:boolean = true;
    public lineChartType:string = 'line';

    public chartClicked(e:any):void {
        console.log(e);
    }

    public chartHovered(e:any):void {
        console.log(e);
    }

    private defaultFuncCallBack(response){
        let newLineChartData = new Array({ 
                                        data: new Array(this.months.length) 
                                    });
        response.forEach(element => {
            newLineChartData[0].data[element.monthNumber-1] = element.count;
        });
        this.lineChartData = newLineChartData;
    }
    /**
     * End LineChart
     */

    /**
     * Controls
     */
    public disabledYear: boolean = false;
    public years: Array<number> = [];
    public selectedYear: number = this.actualDate.getFullYear();
    public selectedYearChange(value: number){
        if(value){
            this.getStatistic(value, (response) => {
                this.defaultFuncCallBack(response);
            });
        }
    }
     
    public scales: Array<any> = [ "Meses", "Años" ];
    public selectedScale: string = "Meses";

    public scaleChange(value){
        if(value === "Años"){
            this.baseApi.getAll(`Complaint/GetComplaintsMadeLineChartAnual`, (response) => {
                if(response.length < 3) 
                    this.setYearsAfterAndBefore(response, 2);
                else 
                    this.setYearsAfterAndBefore(response, 2, false);

                let labels = response.map(x=> x.year);
                let newLineChartData = new Array({ 
                                                    data: new Array() 
                                                });
                response.forEach(element => {
                    newLineChartData[0].data.push(element.count);
                });
                this.lineChartData = newLineChartData;
                this.lineChartLabels =  labels
            });
            this.disabledYear = true;
        }
        else{
            this.getStatistic(this.selectedYear, (response) => {
                this.defaultFuncCallBack(response);
                this.lineChartLabels =  this.months;
            });
            this.disabledYear = false;
        }
    }
    private setYearsAfterAndBefore(response: any, length: number, applyBefore: boolean = true){
        var array = response;
        for (let index = 0; index < length; index++) {
            let lastobj = response[response.length-1]
            response.push({year: lastobj.year+1, count: undefined})
            if(applyBefore){
                let firstobj = response[0];
                response.unshift({year: firstobj.year-1, count: undefined})
            }
        }
    }
    /**
     * End Controls
     */
    constructor(private baseApi: BaseapiService<ComplaintTypeModel>){

    }

    ngOnInit() {
        this.getStatistic(this.actualDate.getFullYear(), (response) => {
             this.defaultFuncCallBack(response);
        });

        this.baseApi.getAll(`Complaint/GetYearsWhereComplaintsWereMade`, (response: Array<number>)=>{
            if(response.indexOf(this.actualDate.getFullYear()) == -1)
                response.unshift(this.actualDate.getFullYear());
            this.years = response;
        });
    }


    private getStatistic(year: number, callback: Function){
        this.baseApi.getAll(`Complaint/GetComplaintsMadeLineChartMonthlyByYear?year=${year}`, callback);
    }
}