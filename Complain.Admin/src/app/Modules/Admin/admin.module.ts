import { NgModule } from '@angular/core';
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { AdminComponent } from "./Components/admin.component";
import { AdminService } from "./Services/admin.service";
import { BroadCastService } from "./Services/broadcast.service";

@NgModule({
  imports:[BrowserModule, FormsModule],
  declarations: [AdminComponent],
  providers: [AdminService, BroadCastService]
})
export class AdminModule {}