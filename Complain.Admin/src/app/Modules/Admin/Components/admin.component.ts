import {Component} from '@angular/core';
import { UserModel } from "../../../Models/UserModel";
import { BroadCastModel } from "../../../Models/BroadCastModel";
import { ResponseModel } from "../../../Models/ResponseModel";
import { BroadCastService } from "../Services/broadcast.service";
import { AdminService } from "../Services/admin.service";
import * as $ from 'jquery';
import * as toastr from 'toastr';

@Component({
    selector:'admin-home',
    templateUrl: `../../Templates/admin.template.html`,
    styleUrls:['../../../../../styles.css']
})

export class AdminComponent{
    isLoading: boolean;
    entityToDelete: UserModel | BroadCastModel;
    users: Array<UserModel>;
    
    
    // Administradores
    administradores: Array<UserModel>;
    adminModel: UserModel;
    saveAdministrador: () => void;
    getAllAdministrador: () => void;

    // BroadCast
    broadCasts: Array<BroadCastModel>;
    broadCastModel: BroadCastModel;
    saveBroadCast: () => void;
    getAllBroadCast: () => void;
    removeBroadCast: (id: string) => void;
    showModalToDeleteBroadCast: (id: string) => void;
    initVmBroadCast: () => void;
    
    ngOnInit(): void {
        this.getAllBroadCast();
        this.getAllUsers();
    }

    constructor(private broadCastService: BroadCastService,
                private adminService: AdminService){
        const self = this;

        self.isLoading = false;
        self.entityToDelete = undefined;


        // Administradores
        self.administradores = new Array();
        self.adminModel = new UserModel();
        self.saveAdministrador = () => {
            
        }   
        self.getAllAdministrador = () => {
            self.isLoading = true;
            var callback = (models: Array<UserModel>, response: ResponseModel<any>, httpResult: any) => {
                self.administradores = models;
                self.isLoading = false;
            }
            var errorcallback = (httpResult: any, response: ResponseModel<any>) => {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            }
            //self.complaintTypeService.getAll(callback, errorcallback)
        }

        // BroadCast
        self.broadCasts = new Array();
        self.broadCastModel = new BroadCastModel();

        self.getAllBroadCast = () => {
            self.isLoading = true;
            var callback = (models: Array<BroadCastModel>, response: ResponseModel<any>, httpResult: any) => {
                self.broadCasts = models;
                self.isLoading = false;
            }
            var errorcallback = (httpResult: any, response: ResponseModel<any>) => {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            }
            self.broadCastService.getAll(callback, errorcallback)
        }

        self.saveBroadCast = () => {
            self.isLoading = true;
            var callback = (model: BroadCastModel, response: ResponseModel<any>, httpResult: any) => {
                toastr["success"]("Registro guardado", "Perfecto");
                self.isLoading = false;
                self.getAllBroadCast();
                $('.modal').modal("hide");
            }
            var errorcallback = (response: ResponseModel<any>, httpResult: any) => {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Hemos tenido un problema al procesar la petición", "Algo salio mal!");
            }

            if (self.broadCastModel.id) {
                //self.broadCastService.update(self.broadCastModel, callback, errorcallback);
            } else {
                self.broadCastService.create(self.broadCastModel, callback, errorcallback);
            }
        }

        self.removeBroadCast = (id) => {
            self.isLoading = true;
            var callback = (model: BroadCastModel, response: ResponseModel<any>, httpResult: any) => {
                toastr["success"]("Registro eliminado", "Perfecto");
                $("#BroadCastModalDelete").modal("hide");
                self.getAllBroadCast();
                self.isLoading = false;
            }
            var errorcallback = (response: ResponseModel<any>, httpResult: any) => {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Hemos tenido un problema al procesar la petición", "Algo salio mal!");
            }
            self.broadCastService.delete(id, callback, errorcallback)
        }

        self.showModalToDeleteBroadCast = (id) => {
            self.isLoading = true;
            var callback = (model: BroadCastModel, response: ResponseModel<any>, httpResult: any) => {
                self.entityToDelete = model;
                self.isLoading = false;
            }
            var errorcallback = (httpResult: any, response: ResponseModel<any>) => {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            }
            self.broadCastService.getBy(id, callback, errorcallback)
        }
        self.initVmBroadCast = () => {
            self.broadCastModel = new BroadCastModel();
            self.isLoading = false;
        }
    }

    getAllUsers(){
        this.isLoading = true;
        var callback = (models: Array<UserModel>, response: ResponseModel<any>, httpResult: any) => {
            this.users = models;
            this.isLoading = false;
        }
        var errorcallback = (httpResult: any, response: ResponseModel<any>) => {
            this.isLoading = false;
            console.log(response);
            toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
        }
        this.adminService.getAllUsers(callback, errorcallback)
    }
} 