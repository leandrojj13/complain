import { NgModule } from '@angular/core';
import { MailboxComponent } from "./Components/mailbox.component";
import { MailboxService } from "./Services/mailbox.service";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { AgmCoreModule } from 'angular2-google-maps/core';

@NgModule({
  imports:[BrowserModule, FormsModule,
   AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDFqMQc4vvUgps29X50MdFSMahwYrFU8jY'
    })],
  declarations: [MailboxComponent],
  providers: [ MailboxService ]
})
export class MailboxModule {}