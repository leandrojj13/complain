import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Headers, Http, RequestOptions } from '@angular/http';
import { ResponseModel } from "../../../Models/ResponseModel";
import { ComplaintTypeModel } from "../../../Models/ComplaintTypeModel";
import { ComplaintModel } from "../../../Models/ComplaintModel";
import { environment } from "../../../Environments/environment"; 
import { StatusComplaint } from "../../../Enums/StatusComplaint";

@Injectable()

export class MailboxService {
    private apiUrl = `${environment.apiUrl}complaint`;;
    private headers = new Headers({ 'Content-Type': 'application/json' });
    private options = new RequestOptions({ headers: this.headers })
    constructor(private http: Http) { }

    getAll(callback: Function, errorcalback?: Function): void {
        this.http.get(this.apiUrl, this.options)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<any>;
                    if (response.success) {
                        var model = httpResult.json().entities as Array<ComplaintModel>;
                        callback(model, response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response, httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(function (error: any) {
                console.log('An error occurred');
                errorcalback(error);
            });
    }

    filterByCode(code: string, callback: Function, errorcalback?: Function): void {
        const url = `${this.apiUrl}/filterByCode/${code.toLowerCase()}`;
        this.http.get(url)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<any>;
                    if (response.success) {
                        var model = httpResult.json().entities as Array<ComplaintModel>;
                        callback(model, response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response, httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(function (error: any) {
                console.log('An error occurred');
                errorcalback(error);
            });
    }
    filterByToday(callback: Function, errorcalback?: Function): void {
        const url = `${this.apiUrl}/filterByToday`;
        this.http.get(url)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<any>;
                    if (response.success) {
                        var model = httpResult.json().entities as Array<ComplaintModel>;
                        callback(model, response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response, httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(function (error: any) {
                console.log('An error occurred');
                errorcalback(error);
            });
    }
    filterByLastWeek(callback: Function, errorcalback?: Function): void {
        const url = `${this.apiUrl}/filterByLastWeek`;
        this.http.get(url)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<any>;
                    if (response.success) {
                        var model = httpResult.json().entities as Array<ComplaintModel>;
                        callback(model, response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response, httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(function (error: any) {
                console.log('An error occurred');
                errorcalback(error);
            });
    }
    filterByRecent(callback: Function, errorcalback?: Function): void {
        const url = `${this.apiUrl}/filterByRecent`;
        this.http.get(url)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<any>;
                    if (response.success) {
                        var model = httpResult.json().entities as Array<ComplaintModel>;
                        callback(model, response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response, httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(function (error: any) {
                console.log('An error occurred');
                errorcalback(error);
            });
    }
    filterByPending(callback: Function, errorcalback?: Function): void {
        const url = `${this.apiUrl}/filterByPending`;
        this.http.get(url)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<any>;
                    if (response.success) {
                        var model = httpResult.json().entities as Array<ComplaintModel>;
                        callback(model, response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response, httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(function (error: any) {
                console.log('An error occurred');
                errorcalback(error);
            });
    }
    filterByAttended(callback: Function, errorcalback?: Function): void {
        const url = `${this.apiUrl}/filterByAttended`;
        this.http.get(url)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<any>;
                    if (response.success) {
                        var model = httpResult.json().entities as Array<ComplaintModel>;
                        callback(model, response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response, httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(function (error: any) {
                console.log('An error occurred');
                errorcalback(error);
            });
    }
    getBy(id: string, callback: Function, errorcalback?: Function): void {
        const url = `${this.apiUrl}/${id}`;
        this.http.get(url, this.options)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<any>;
                    if (response.success) {
                        var model = httpResult.json().entity as ComplaintModel;
                        callback(model, response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response, httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(function (error: any) {
                console.log('An error occurred');
                errorcalback(error);
            });
    }
    ChangeStatus(id: string, status: StatusComplaint, callback: Function, errorcalback?: Function): void {
        const url = `${this.apiUrl}/ChangeStatus/${id}/${status}`;
        this.http.get(url, this.options)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<any>;
                    if (response.success) {
                        var model = httpResult.json().entity as ComplaintModel;
                        callback(model, response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response, httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(function (error: any) {
                console.log('An error occurred');
                errorcalback(error);
            });
    }
}