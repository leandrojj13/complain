import { Component } from '@angular/core';
import { ComplaintModel } from "../../../Models/ComplaintModel";
import { MailboxService } from "../Services/mailbox.service";
import { ResponseModel } from "../../../Models/ResponseModel";
import { StatusComplaint } from "../../../Enums/StatusComplaint";
import { AppComponent }  from '../../../app.component';
import * as $ from 'jquery';
import * as toastr from 'toastr';

@Component({
    selector: 'mail-box',
    templateUrl: `../../Templates/mailbox.template.html`,
    styleUrls: ['../../../../../styles.css']
})

export class MailboxComponent {
    complaints: Array<ComplaintModel>;
    complaintSelected: ComplaintModel;
    filterCode: string;
    zoom: number;
    getAll: () => void;
    initVm: () => void;
    filterByCode: (code: string) => void;
    filterByAttended: () => void;
    filterByPending: () => void;
    filterByRecent: () => void;
    filterByLastWeek: () => void;
    filterByToday: () => void;
    selectComplaint: (item: ComplaintModel) => void;
    getSelectedCompaint: (id: string) => void;
    ChangeStatus: (id: string, status: StatusComplaint) => void;
    constructor(private maialboxService: MailboxService) {
        const self = this;
        self.zoom = 18;
        const callback = (models, response) => {
            self.complaints = models;
        }
        const errorcallback = (httpResult: any, response: ResponseModel<any>) => {
            toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
        }
        self.complaints = new Array<ComplaintModel>();
        self.complaintSelected = undefined;

        self.getAll = () => {
            self.maialboxService.getAll(callback, errorcallback)
        }

        self.getSelectedCompaint = (id) => {
            var callback = (model, response) => {
                self.complaintSelected = model;
                if(  self.complaintSelected.latitude &&  self.complaintSelected.longitude){
                    self.complaintSelected.latitude = Number.parseFloat(self.complaintSelected.latitude.toString());
                    self.complaintSelected.longitude = Number.parseFloat(self.complaintSelected.longitude.toString());
                 //   AppComponent.tick() 
                    var attachaments = self.complaintSelected["attachments"];
                    var newArray = [];
                    for (var index = 0; index < attachaments.length; index++) {
                        var element = attachaments[index];
                        if(!element.name.includes("Foto")){
                            newArray.push(element);
                        }
                    }
                    self.complaintSelected["attachments"] = newArray;
                }
            }
            var errorcallback = (httpResult: any, response: ResponseModel<any>) => {
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            }
            maialboxService.getBy(id, callback, errorcallback);
        }

        self.selectComplaint = (item) => {
            if (item) {
                self.getSelectedCompaint(item.id);
            }
        }
        self.filterByCode = (code) => {
            if (code) {
                maialboxService.filterByCode(code, callback, errorcallback);
            } else {
                self.filterByRecent();
            }

        }
        self.filterByRecent = () => {
            maialboxService.filterByRecent(callback, errorcallback);
        }
        self.filterByAttended = () => {
            maialboxService.filterByAttended(callback, errorcallback);
        }
        self.filterByPending = () => {
            maialboxService.filterByPending(callback, errorcallback);
        }
        self.filterByToday = () => {
            maialboxService.filterByToday(callback, errorcallback);
        }
        self.filterByLastWeek = () => {
            maialboxService.filterByLastWeek(callback, errorcallback);
        }
        self.ChangeStatus = (id: string, status: StatusComplaint) => {
            var callback = (model, response) => {
                self.complaintSelected = model;
                self.filterByRecent();
            }
            var errorcallback = (httpResult: any, response: ResponseModel<any>) => {
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            }
            maialboxService.ChangeStatus(id, status, callback, errorcallback);
        }
        self.initVm = () => {
        }

    }
    ngOnInit(): void {
        this.filterByRecent();
    }
} 