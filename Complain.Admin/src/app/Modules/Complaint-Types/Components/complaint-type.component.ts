import { Component, OnInit } from '@angular/core';
import { ComplaintTypeModel } from "../../../Models/ComplaintTypeModel";
import { ResponseModel } from "../../../Models/ResponseModel";
import { ComplaintTypeService } from "../Services/complaint-type.service";
import * as $ from 'jquery';
import * as toastr from 'toastr';

@Component({
    selector: 'complaint-types',
    templateUrl: `../../Templates/complaint-type.template.html`,
    styleUrls: ['../../../../../styles.css']
})

export class ComplaintTypeComponent implements OnInit {
    complaintTypes: Array<ComplaintTypeModel>;
    model: ComplaintTypeModel;
    entityToDelete: ComplaintTypeModel;
    previewImage: string;
    fileReader: FileReader;
    isLoading: boolean;
    getAll: () => void;
    initVm: () => void;
    save: () => void;    
    loadImage: (event:any) => void;
    remove: (id: string) => void;
    showModalToDelete: (id: string) => void;
    showModalToEdit: (id: string) => void;
    constructor(private complaintTypeService: ComplaintTypeService) {
        const self = this;
        self.previewImage = "../../content/camera.ico";        
        self.complaintTypes = new Array();
        self.model = new ComplaintTypeModel();
        self.entityToDelete = undefined;
        self.model.imageUrl = self.previewImage;
        self.isLoading = false;
        self.fileReader = new FileReader();
        self.fileReader.onload = (event: any) => {
            self.model.imageUrl = event.target.result;
        }
        self.getAll = () => {
            self.isLoading = true;
            var callback = (models: Array<ComplaintTypeModel>, response: ResponseModel<any>, httpResult: any) => {
                self.complaintTypes = models;
                self.isLoading = false;
            }
            var errorcallback = (httpResult: any, response: ResponseModel<any>) => {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            }
            self.complaintTypeService.getAll(callback, errorcallback)
        }
        self.save = () => {
            self.isLoading = true;
            var callback = (model: ComplaintTypeModel, response: ResponseModel<any>, httpResult: any) => {
                toastr["success"]("Registro guardado", "Perfecto");
                self.isLoading = false;
                self.getAll();
                $('.modal').modal("hide");
            }
            var errorcallback = (response: ResponseModel<any>, httpResult: any) => {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Hemos tenido un problema al procesar la petición", "Algo salio mal!");
            }

            if (self.model.id) {
                self.complaintTypeService.update(self.model, callback, errorcallback);
            } else {
                self.complaintTypeService.create(self.model, callback, errorcallback);
            }
        }
        self.remove = (id) => {
            self.isLoading = true;
            var callback = (model: ComplaintTypeModel, response: ResponseModel<any>, httpResult: any) => {
                toastr["success"]("Registro eliminado", "Perfecto");
                $("#ComplaintTypeModalDelete").modal("hide");
                self.getAll();
                self.isLoading = false;
            }
            var errorcallback = (response: ResponseModel<any>, httpResult: any) => {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Hemos tenido un problema al procesar la petición", "Algo salio mal!");
            }
            self.complaintTypeService.delete(id, callback, errorcallback)
        }
        self.showModalToDelete = (id) => {
            self.isLoading = true;
            var callback = (model: ComplaintTypeModel, response: ResponseModel<any>, httpResult: any) => {
                self.entityToDelete = model;
                self.isLoading = false;
            }
            var errorcallback = (httpResult: any, response: ResponseModel<any>) => {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            }
            self.complaintTypeService.getBy(id, callback, errorcallback)
        }
        self.showModalToEdit = (id) => {
            self.isLoading = true;
            var callback = (model: ComplaintTypeModel, response: ResponseModel<any>, httpResult: any) => {
                self.model = model;
                self.isLoading = false;
            }
            var errorcallback = (httpResult: any, response: ResponseModel<any>) => {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            }
            self.complaintTypeService.getBy(id, callback, errorcallback)
        }
        self.initVm = () => {
            self.model = new ComplaintTypeModel();
            self.model.imageUrl = self.previewImage;
            self.isLoading = false;
        }
        self.loadImage = (event)=> {
            if (event.target.files && event.target.files[0]) {
                self.model.image = event.target.files[0];
                self.fileReader.readAsDataURL(event.target.files[0]);
            }
        }
    }
    ngOnInit(): void {
        this.getAll();
    }
    initInputFile() {
        $("#inputfileInput").click();
    }
}