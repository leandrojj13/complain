import { NgModule }             from '@angular/core';
import { ComplaintTypeComponent } from "./Components/complaint-type.component";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { ComplaintTypeService } from "./Services/complaint-type.service";


@NgModule({
    imports:[BrowserModule, FormsModule],
    declarations: [ComplaintTypeComponent],
    providers: [ComplaintTypeService]

})
export class ComplaintTypeModule {

    constructor(){

    }
}