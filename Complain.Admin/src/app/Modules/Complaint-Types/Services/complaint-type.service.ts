import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Headers, Http, RequestOptions } from '@angular/http';
import { ResponseModel } from "../../../Models/ResponseModel";
import { ComplaintTypeModel } from "../../../Models/ComplaintTypeModel";
import { environment } from "../../../Environments/environment"; 
@Injectable()

export class ComplaintTypeService {
    private apiUrl = `${environment.apiUrl}complaintType`;;
    private headers = new Headers({ 'Content-Type': 'application/json' });
    private options = new RequestOptions({ headers: this.headers })
    constructor(private http: Http) { }

    getAll(callback: Function, errorcalback?: Function): void {
        this.http.get(this.apiUrl, this.options)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<any>;
                    if (response.success) {
                        var model = httpResult.json().entities as Array<ComplaintTypeModel>;
                        callback(model,response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response,httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(function(error:any){
                 console.log('An error occurred'); 
                 errorcalback(error);
            });
    }
    getBy(id: string, callback: Function, errorcalback?: Function): void {
        const url = `${this.apiUrl}/${id}`;
        this.http.get(url, this.options)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<any>;
                    if (response.success) {
                        var model = httpResult.json().entity as ComplaintTypeModel;
                        callback(model, response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response, httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(function(error:any){
                 console.log('An error occurred'); 
                 errorcalback(error);
            });
    }
    create(model: ComplaintTypeModel, callback: Function, errorcalback?: Function): void {
        //const options = new RequestOptions({ headers: new Headers({ 'Content-Type': 'multipart/form-data' }) });
        var data = new FormData();
        data.append("model", JSON.stringify(model));
        data.append("image", model.image);

        this.http.post(this.apiUrl + "/Create", data)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<any>;
                    if (response.success) {
                        var model = httpResult.json().entity as ComplaintTypeModel;
                        callback(model,response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response,httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
                })
            .catch(function(error:any){
                 console.log('An error occurred'); 
                 errorcalback(error);
            });
    }
    update(model: ComplaintTypeModel, callback: Function, errorcalback?: Function): void {
        const url = `${this.apiUrl}/Update/${model.id}`;
        //const options = new RequestOptions({ headers: new Headers({ 'Content-Type': 'multipart/form-data' }) });
        
        var data = new FormData();
        data.append("model", JSON.stringify(model));
        data.append("image", model.image);

        this.http.put(url, data)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<any>;
                    if (response.success) {
                        var model = httpResult.json().entity as ComplaintTypeModel;
                        callback(model,response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response,httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(function(error:any){
                 console.error('An error occurred', error); 
                 errorcalback(error);
            });
    }
    delete(id: string, callback: Function, errorcalback?: Function): void {
        const url = `${this.apiUrl}/${id}`;
        this.http.delete(url, this.options)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<any>;
                    if (response.success) {
                        var model = httpResult.json().entity as ComplaintTypeModel;
                        callback(model,response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response,httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(function(error:any){
                 console.error('An error occurred', error); 
                 errorcalback(error);
            });
    }
}