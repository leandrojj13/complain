import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `<main-layout></main-layout>`,
  styles: ["../styles.css"]
})
export class AppComponent  {

  
}
