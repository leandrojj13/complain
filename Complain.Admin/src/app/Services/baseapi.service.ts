import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Headers, Http, RequestOptions } from '@angular/http';
import { BaseModel } from "../Models/BaseModel";
import { ResponseModel } from "../Models/ResponseModel";
import { environment } from "../Environments/environment"; 

@Injectable()

export class BaseapiService<T extends BaseModel> {
    private apiUrl = environment.apiUrl;

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
    private headers = new Headers({ 'Content-Type': 'application/json' });
    private options = new RequestOptions({ headers: this.headers })
    constructor(private http: Http) { }

    getAll(url:string, callback: Function, errorcalback?: Function): void {
        this.http.get(`${this.apiUrl}${url}` , this.options)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<T>;
                    if (response.success) {
                        var model = response.entities;
                        callback(model, response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response, httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(this.handleError);
    }
    get(id: string, url:string, callback: Function, errorcalback?: Function): void {
        this.http.get(`${this.apiUrl}${url}/${id}`, this.options)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<T>;
                    if (response.success) {
                        var model = response.entity;
                        callback(model, response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response, httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(this.handleError);
    }
    post(url:string, data: any, callback: Function, errorcalback?: Function): void {
        this.http
            .post(`${this.apiUrl}${url}`, JSON.stringify(data), this.options)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<T>;
                    if (response.success) {
                        var model = response.entity;
                        callback(model, response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response, httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(this.handleError);
    }
    put(url:string,data: T, callback: Function, errorcalback?: Function): void {
        this.http
            .put( `${this.apiUrl}${url}/${data.id}`, JSON.stringify(data), this.options)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<T>;
                    if (response.success) {
                        var model = response.entity;
                        callback(model, response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response, httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(this.handleError);
    }
    delete(id: string,url:string, callback: Function, errorcalback?: Function): void {
        this.http.delete(`${this.apiUrl}${url}/${id}`, this.options)
            .toPromise()
            .then(httpResult => {
                if (httpResult.ok) {
                    const response = httpResult.json() as ResponseModel<T>;
                    if (response.success) {
                        var model = response.entity as T;
                        callback(model, response, httpResult);
                    } else {
                        if (errorcalback) {
                            errorcalback(response, httpResult);
                        }
                    }
                } else {
                    if (errorcalback) {
                        errorcalback(httpResult);
                    }
                }
            })
            .catch(this.handleError);
    }
}