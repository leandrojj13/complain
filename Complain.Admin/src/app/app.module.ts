import { NgModule,enableProdMode }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from "./app-routing.module";
import { ComplaintTypeModule } from "./Modules/Complaint-Types/complaint-type.module";
import { HomeModule } from "./Modules/Home/home.modules";
import { MailboxModule } from "./Modules/Mailbox/maibox.module";
import { AdminModule } from "./Modules/Admin/admin.module";
import { HttpModule } from "@angular/http";

import { AppComponent }  from './app.component';
import { LayoutComponent } from "./Components/layout.component";
import { MenuComponent } from "./Components/menu.component";

import { BaseapiService } from "./Services/baseapi.service";

//enableProdMode();

@NgModule({
  imports:      [ 
    BrowserModule, 
    AppRoutingModule,
    ComplaintTypeModule,
    HomeModule,
    MailboxModule,
    AdminModule,
    HttpModule,
    
    ],
  declarations: [ 
      AppComponent, 
      LayoutComponent,
      MenuComponent
     ],
  providers: [BaseapiService],  
  bootstrap:    [ 
    AppComponent 
    ]
})
export class AppModule { }
