import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from "./Modules/Home/Components/home.component";
import { ComplaintTypeComponent } from "./Modules/Complaint-Types/Components/complaint-type.component";
import { MailboxComponent } from "./Modules/Mailbox/Components/mailbox.component";
import { AdminComponent } from "./Modules/Admin/Components/admin.component";

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home',  component: HomeComponent },
  { path: 'complaintTypes',  component: ComplaintTypeComponent },
  { path: 'admin',  component: AdminComponent },
  { path: 'mailbox',  component: MailboxComponent },
  { path: '**',  redirectTo: '/home' },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}