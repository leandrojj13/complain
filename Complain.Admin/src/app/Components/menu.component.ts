import { Component } from '@angular/core';
import { MenuItemModel, MENUITEMS } from "../Models/MenuItemModel";

@Component({
    selector: 'app-menu',
    templateUrl: `../../Templates/menu.template.html`,
    styleUrls: ['../../../styles.css']
})

export class MenuComponent {
    menuItems: Array<MenuItemModel>;
    title: string;
    rootPath: string;
    constructor() {
        this.menuItems = MENUITEMS;
        this.title = "Complaint";
        this.rootPath = "";       
    }
} 