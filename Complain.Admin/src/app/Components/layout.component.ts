import {Component} from '@angular/core';

@Component({
    selector:'main-layout',
    templateUrl: `../../Templates/layout.template.html`,
    styleUrls:['../../../styles.css']
})

export class LayoutComponent{
    private age:number;
    constructor(){
       this.age = new Date().getFullYear();      
    }
} 