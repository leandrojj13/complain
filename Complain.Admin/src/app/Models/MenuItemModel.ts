export class MenuItemModel{
    name:string;
    path:string;
}

export const MENUITEMS: MenuItemModel[] = [
  {path: "mailbox", name: 'Bandeja de entrada'},  
  {path: "complaintTypes", name: 'Tipos de denuncias'},
  {path: "admin", name: 'Administración'},
];