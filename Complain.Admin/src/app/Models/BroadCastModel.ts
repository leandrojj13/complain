import { BaseModel } from "./BaseModel";

export class BroadCastModel extends BaseModel {
    title: string;
    message: string;
    userId: string;
    isAll: boolean;

    constructor(){
        super();
        this.title = "";
        this.message = "";  
        this.userId = "";  
        this.isAll = false;  
    }
}