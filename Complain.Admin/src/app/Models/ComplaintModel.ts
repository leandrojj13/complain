import { BaseModel } from "./BaseModel";
import { AddressType } from "../Enums/AddressType";
import { StatusComplaint } from "../Enums/StatusComplaint";

export class ComplaintModel extends BaseModel {
    userId: string;
    description:string;
    complaintTypeId: string;
    complaintTypeName:string;
    sector:string;
    street:string;
    codigo:string;
    addressDescription:string;
    residencia:string;
    addressType:AddressType;
    status:StatusComplaint;
    latitude: number;
    longitude: number;
    constructor(){
        super();
        this.complaintTypeId = undefined;
        this.description = undefined;
        this.complaintTypeName = undefined;
        this.status = undefined;    
        this.latitude = 0;    
        this.longitude = 0;    
    }
}