export class ResponseModel<T>{
    entities: Array<T>;
    entity: T;
    message:string;
    success:boolean;
    statusCode:number;
}