import { BaseModel } from "./BaseModel";

export class UserModel extends BaseModel {
    name: string;
    email: string;

    constructor(){
        super();
        this.name = "";
        this.email = "";  
    }
}