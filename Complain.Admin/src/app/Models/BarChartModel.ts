import { BaseModel } from "./BaseModel";

export class BarChartModel extends BaseModel {
    name:string;
    count: number
    constructor(){
        super();
        this.name = undefined;
        this.count = undefined;
    }
}
