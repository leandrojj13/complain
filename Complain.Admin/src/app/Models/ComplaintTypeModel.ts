import { BaseModel } from "./BaseModel";

export class ComplaintTypeModel extends BaseModel {
    name:string;
    description:string;
    imageUrl: string;
    codigo:string;
    image:File;
    constructor(){
        super();
        this.name = "";
        this.description = "";
        this.imageUrl = "";
        this.codigo = "";    
        this.image = undefined;    
    }
   
}
