/**
 * System configuration for Angular samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
  System.config({
    baseUrl: 'node_modules/',
    paths: {
      // paths serve as alias
      'npm:': 'node_modules/'
    },
    // map tells the System loader where to look for things
    map: {
      // our app is within the app folder
      'app': 'app',
      // angular bundles
      '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
      '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
      '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
      '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
      '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
      '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
      '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
      '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',

      // other libraries  
      'rxjs': 'npm:rxjs',
      'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
      'angular2-google-maps/core': 'npm:angular2-google-maps/core/core.umd.js',
      'angular2-jwt': 'npm:angular2-jwt/angular2-jwt.js',
      'ng2-toastr': 'npm:ng2-toastr/ng2-toastr.js',
      'ng2-file-input': 'npm:ng2-file-input/dist/ng2-file-input',
      'chart.js': 'npm:chart.js/dist/chart.js',
      'mydatepicker': 'npm:mydatepicker/bundles/mydatepicker.umd.min.js',
      'moment': 'npm:moment/min',
      'jquery': 'npm:jquery/dist/jquery.min.js',
      'bootstrap': 'npm:bootstrap/dist/js/bootstrap.min.js',
      'toastr': 'npm:toastr/build/toastr.min.js'
    },

    // packages tells the System loader how to load when no filename and/or no extension
    packages: {
      app: {
        defaultExtension: 'js',
        meta: {
          './*.js': {
            loader: 'systemjs-angular-loader.js'
          }
        }
      },
      'moment': {
        main: './moment-with-locales.min.js',
        defaultExtension: 'js'
      },
      rxjs: {
        defaultExtension: 'js'
      }
    }
  });
  System.import("jquery").then(function (m) {
    System.import("bootstrap")
  });
})(this);
