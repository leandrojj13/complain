"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseModel_1 = require("./BaseModel");
var ComplaintTypeModel = (function (_super) {
    __extends(ComplaintTypeModel, _super);
    function ComplaintTypeModel() {
        var _this = _super.call(this) || this;
        _this.name = "";
        _this.description = "";
        _this.imageUrl = "";
        _this.codigo = "";
        _this.image = undefined;
        return _this;
    }
    return ComplaintTypeModel;
}(BaseModel_1.BaseModel));
exports.ComplaintTypeModel = ComplaintTypeModel;
//# sourceMappingURL=ComplaintTypeModel.js.map