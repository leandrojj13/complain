"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseModel_1 = require("./BaseModel");
var ComplaintModel = (function (_super) {
    __extends(ComplaintModel, _super);
    function ComplaintModel() {
        var _this = _super.call(this) || this;
        _this.complaintTypeId = undefined;
        _this.description = undefined;
        _this.complaintTypeName = undefined;
        _this.status = undefined;
        _this.latitude = 0;
        _this.longitude = 0;
        return _this;
    }
    return ComplaintModel;
}(BaseModel_1.BaseModel));
exports.ComplaintModel = ComplaintModel;
//# sourceMappingURL=ComplaintModel.js.map