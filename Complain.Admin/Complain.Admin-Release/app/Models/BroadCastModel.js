"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseModel_1 = require("./BaseModel");
var BroadCastModel = (function (_super) {
    __extends(BroadCastModel, _super);
    function BroadCastModel() {
        var _this = _super.call(this) || this;
        _this.title = "";
        _this.message = "";
        _this.userId = "";
        _this.isAll = false;
        return _this;
    }
    return BroadCastModel;
}(BaseModel_1.BaseModel));
exports.BroadCastModel = BroadCastModel;
//# sourceMappingURL=BroadCastModel.js.map