"use strict";
var MenuItemModel = (function () {
    function MenuItemModel() {
    }
    return MenuItemModel;
}());
exports.MenuItemModel = MenuItemModel;
exports.MENUITEMS = [
    { path: "mailbox", name: 'Bandeja de entrada' },
    { path: "complaintTypes", name: 'Tipos de denuncias' },
    { path: "admin", name: 'Administración' },
];
//# sourceMappingURL=MenuItemModel.js.map