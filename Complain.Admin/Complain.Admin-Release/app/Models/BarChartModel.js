"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseModel_1 = require("./BaseModel");
var BarChartModel = (function (_super) {
    __extends(BarChartModel, _super);
    function BarChartModel() {
        var _this = _super.call(this) || this;
        _this.name = undefined;
        _this.count = undefined;
        return _this;
    }
    return BarChartModel;
}(BaseModel_1.BaseModel));
exports.BarChartModel = BarChartModel;
//# sourceMappingURL=BarChartModel.js.map