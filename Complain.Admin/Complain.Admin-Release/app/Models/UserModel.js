"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseModel_1 = require("./BaseModel");
var UserModel = (function (_super) {
    __extends(UserModel, _super);
    function UserModel() {
        var _this = _super.call(this) || this;
        _this.name = "";
        _this.email = "";
        return _this;
    }
    return UserModel;
}(BaseModel_1.BaseModel));
exports.UserModel = UserModel;
//# sourceMappingURL=UserModel.js.map