"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
require("rxjs/add/operator/toPromise");
var http_1 = require("@angular/http");
var environment_1 = require("../Environments/environment");
var BaseapiService = (function () {
    function BaseapiService(http) {
        this.http = http;
        this.apiUrl = environment_1.environment.apiUrl;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.options = new http_1.RequestOptions({ headers: this.headers });
    }
    BaseapiService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    BaseapiService.prototype.getAll = function (url, callback, errorcalback) {
        this.http.get("" + this.apiUrl + url, this.options)
            .toPromise()
            .then(function (httpResult) {
            if (httpResult.ok) {
                var response = httpResult.json();
                if (response.success) {
                    var model = response.entities;
                    callback(model, response, httpResult);
                }
                else {
                    if (errorcalback) {
                        errorcalback(response, httpResult);
                    }
                }
            }
            else {
                if (errorcalback) {
                    errorcalback(httpResult);
                }
            }
        })
            .catch(this.handleError);
    };
    BaseapiService.prototype.get = function (id, url, callback, errorcalback) {
        this.http.get("" + this.apiUrl + url + "/" + id, this.options)
            .toPromise()
            .then(function (httpResult) {
            if (httpResult.ok) {
                var response = httpResult.json();
                if (response.success) {
                    var model = response.entity;
                    callback(model, response, httpResult);
                }
                else {
                    if (errorcalback) {
                        errorcalback(response, httpResult);
                    }
                }
            }
            else {
                if (errorcalback) {
                    errorcalback(httpResult);
                }
            }
        })
            .catch(this.handleError);
    };
    BaseapiService.prototype.post = function (url, data, callback, errorcalback) {
        this.http
            .post("" + this.apiUrl + url, JSON.stringify(data), this.options)
            .toPromise()
            .then(function (httpResult) {
            if (httpResult.ok) {
                var response = httpResult.json();
                if (response.success) {
                    var model = response.entity;
                    callback(model, response, httpResult);
                }
                else {
                    if (errorcalback) {
                        errorcalback(response, httpResult);
                    }
                }
            }
            else {
                if (errorcalback) {
                    errorcalback(httpResult);
                }
            }
        })
            .catch(this.handleError);
    };
    BaseapiService.prototype.put = function (url, data, callback, errorcalback) {
        this.http
            .put("" + this.apiUrl + url + "/" + data.id, JSON.stringify(data), this.options)
            .toPromise()
            .then(function (httpResult) {
            if (httpResult.ok) {
                var response = httpResult.json();
                if (response.success) {
                    var model = response.entity;
                    callback(model, response, httpResult);
                }
                else {
                    if (errorcalback) {
                        errorcalback(response, httpResult);
                    }
                }
            }
            else {
                if (errorcalback) {
                    errorcalback(httpResult);
                }
            }
        })
            .catch(this.handleError);
    };
    BaseapiService.prototype.delete = function (id, url, callback, errorcalback) {
        this.http.delete("" + this.apiUrl + url + "/" + id, this.options)
            .toPromise()
            .then(function (httpResult) {
            if (httpResult.ok) {
                var response = httpResult.json();
                if (response.success) {
                    var model = response.entity;
                    callback(model, response, httpResult);
                }
                else {
                    if (errorcalback) {
                        errorcalback(response, httpResult);
                    }
                }
            }
            else {
                if (errorcalback) {
                    errorcalback(httpResult);
                }
            }
        })
            .catch(this.handleError);
    };
    return BaseapiService;
}());
BaseapiService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], BaseapiService);
exports.BaseapiService = BaseapiService;
//# sourceMappingURL=baseapi.service.js.map