"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var app_routing_module_1 = require("./app-routing.module");
var complaint_type_module_1 = require("./Modules/Complaint-Types/complaint-type.module");
var home_modules_1 = require("./Modules/Home/home.modules");
var maibox_module_1 = require("./Modules/Mailbox/maibox.module");
var admin_module_1 = require("./Modules/Admin/admin.module");
var http_1 = require("@angular/http");
var app_component_1 = require("./app.component");
var layout_component_1 = require("./Components/layout.component");
var menu_component_1 = require("./Components/menu.component");
var baseapi_service_1 = require("./Services/baseapi.service");
//enableProdMode();
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            app_routing_module_1.AppRoutingModule,
            complaint_type_module_1.ComplaintTypeModule,
            home_modules_1.HomeModule,
            maibox_module_1.MailboxModule,
            admin_module_1.AdminModule,
            http_1.HttpModule,
        ],
        declarations: [
            app_component_1.AppComponent,
            layout_component_1.LayoutComponent,
            menu_component_1.MenuComponent
        ],
        providers: [baseapi_service_1.BaseapiService],
        bootstrap: [
            app_component_1.AppComponent
        ]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map