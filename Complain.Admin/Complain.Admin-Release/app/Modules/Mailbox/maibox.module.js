"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var mailbox_component_1 = require("./Components/mailbox.component");
var mailbox_service_1 = require("./Services/mailbox.service");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var core_2 = require("angular2-google-maps/core");
var MailboxModule = (function () {
    function MailboxModule() {
    }
    return MailboxModule;
}());
MailboxModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, forms_1.FormsModule,
            core_2.AgmCoreModule.forRoot({
                apiKey: 'AIzaSyDFqMQc4vvUgps29X50MdFSMahwYrFU8jY'
            })],
        declarations: [mailbox_component_1.MailboxComponent],
        providers: [mailbox_service_1.MailboxService]
    })
], MailboxModule);
exports.MailboxModule = MailboxModule;
//# sourceMappingURL=maibox.module.js.map