"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var mailbox_service_1 = require("../Services/mailbox.service");
var toastr = require("toastr");
var MailboxComponent = (function () {
    function MailboxComponent(maialboxService) {
        this.maialboxService = maialboxService;
        var self = this;
        self.zoom = 18;
        var callback = function (models, response) {
            self.complaints = models;
        };
        var errorcallback = function (httpResult, response) {
            toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
        };
        self.complaints = new Array();
        self.complaintSelected = undefined;
        self.getAll = function () {
            self.maialboxService.getAll(callback, errorcallback);
        };
        self.getSelectedCompaint = function (id) {
            var callback = function (model, response) {
                self.complaintSelected = model;
                if (self.complaintSelected.latitude && self.complaintSelected.longitude) {
                    self.complaintSelected.latitude = Number.parseFloat(self.complaintSelected.latitude.toString());
                    self.complaintSelected.longitude = Number.parseFloat(self.complaintSelected.longitude.toString());
                    //   AppComponent.tick() 
                    var attachaments = self.complaintSelected["attachments"];
                    var newArray = [];
                    for (var index = 0; index < attachaments.length; index++) {
                        var element = attachaments[index];
                        if (!element.name.includes("Foto")) {
                            newArray.push(element);
                        }
                    }
                    self.complaintSelected["attachments"] = newArray;
                }
            };
            var errorcallback = function (httpResult, response) {
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            };
            maialboxService.getBy(id, callback, errorcallback);
        };
        self.selectComplaint = function (item) {
            if (item) {
                self.getSelectedCompaint(item.id);
            }
        };
        self.filterByCode = function (code) {
            if (code) {
                maialboxService.filterByCode(code, callback, errorcallback);
            }
            else {
                self.filterByRecent();
            }
        };
        self.filterByRecent = function () {
            maialboxService.filterByRecent(callback, errorcallback);
        };
        self.filterByAttended = function () {
            maialboxService.filterByAttended(callback, errorcallback);
        };
        self.filterByPending = function () {
            maialboxService.filterByPending(callback, errorcallback);
        };
        self.filterByToday = function () {
            maialboxService.filterByToday(callback, errorcallback);
        };
        self.filterByLastWeek = function () {
            maialboxService.filterByLastWeek(callback, errorcallback);
        };
        self.ChangeStatus = function (id, status) {
            var callback = function (model, response) {
                self.complaintSelected = model;
                self.filterByRecent();
            };
            var errorcallback = function (httpResult, response) {
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            };
            maialboxService.ChangeStatus(id, status, callback, errorcallback);
        };
        self.initVm = function () {
        };
    }
    MailboxComponent.prototype.ngOnInit = function () {
        this.filterByRecent();
    };
    return MailboxComponent;
}());
MailboxComponent = __decorate([
    core_1.Component({
        selector: 'mail-box',
        templateUrl: "../../Templates/mailbox.template.html",
        styleUrls: ['../../../../../styles.css']
    }),
    __metadata("design:paramtypes", [mailbox_service_1.MailboxService])
], MailboxComponent);
exports.MailboxComponent = MailboxComponent;
//# sourceMappingURL=mailbox.component.js.map