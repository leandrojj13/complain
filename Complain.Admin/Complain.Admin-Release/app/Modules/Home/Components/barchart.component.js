"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var baseapi_service_1 = require("../../../Services/baseapi.service");
var moment = require("moment");
var BarChartComponent = (function () {
    /**
     * Controls
     */
    function BarChartComponent(baseApi) {
        this.baseApi = baseApi;
        /** End DatePicker */
        /** BarChart */
        this.barChartColors = [
            {
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "#18bc9c",
                hoverBackgroundColor: "rgba(24, 188, 156, 0.46)",
                hoverBorderColor: "#18bc9c"
            }
        ];
        this.barChartLabels = [];
        this.barChartData = [];
        this.barChartType = 'horizontalBar';
        this.barChartLegend = true;
        this.barChartDatasets = [
            {
                label: " Denuncias",
                borderWidth: 2
            }
        ];
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true,
            scales: {
                xAxes: [{
                        ticks: {
                            min: 0,
                            suggestedMax: 10,
                            stepSize: 1
                        }
                    }]
            }
        };
        moment.locale('es');
        this.actualDate = moment();
        this.myDatePickerOptions = {
            // other options...
            dateFormat: 'dd.mm.yyyy',
            inline: true
        };
        this.localeDatePicker = "es";
        this.myDatePickerModel = { date: {
                year: this.actualDate.year(),
                month: this.actualDate.month() + 1,
                day: this.actualDate.date()
            }
        };
        this.filterTypes = new Array("Fecha", "Todos");
        this.selectedFilter = "Fecha";
    }
    ;
    BarChartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getStatistic(this.actualDate.toDate(), function (response) {
            var complaintTypesNames = response.map(function (x) { return x.name; });
            var data = response.map(function (x) { return x.count; });
            _this.barChartLabels = complaintTypesNames;
            _this.barChartData = data;
        });
    };
    // events
    BarChartComponent.prototype.chartClicked = function (e) {
        console.log(e);
    };
    BarChartComponent.prototype.chartHovered = function (e) {
        console.log(e);
    };
    BarChartComponent.prototype.selectedFilterChange = function (value) {
        var _this = this;
        if (value == "Todos") {
            this.getStatistic(null, function (response) {
                var data = response.map(function (x) { return x.count; });
                _this.barChartData = data;
            });
        }
        else {
            var date = new Date(this.myDatePickerModel.date.year, this.myDatePickerModel.date.month - 1, this.myDatePickerModel.date.day);
            this.actualDate = moment(date);
            this.getStatistic(date, function (response) {
                var data = response.map(function (x) { return x.count; });
                _this.barChartData = data;
            });
        }
    };
    // dateChanged callback function called when the user select the date. This is mandatory callback
    // in this option. There are also optional inputFieldChanged and calendarViewChanged callbacks.
    BarChartComponent.prototype.onDateChanged = function (event) {
        var _this = this;
        if (event.jsdate) {
            this.actualDate = moment(event.jsdate);
            this.getStatistic(event.jsdate, function (response) {
                var data = response.map(function (x) { return x.count; });
                _this.barChartData = data;
            });
        }
        // event properties are: event.date, event.jsdate, event.formatted and event.epoc
    };
    BarChartComponent.prototype.getStatistic = function (filterDate, callback) {
        this.baseApi.getAll("Complaint/GetComplaintsMadeBarChart?filterDate=" + (filterDate ? filterDate.toUTCString() : filterDate), callback);
    };
    return BarChartComponent;
}());
BarChartComponent = __decorate([
    core_1.Component({
        selector: 'barchart-statistics',
        templateUrl: "../../Templates/barchart.template.html"
    }),
    __metadata("design:paramtypes", [baseapi_service_1.BaseapiService])
], BarChartComponent);
exports.BarChartComponent = BarChartComponent;
//# sourceMappingURL=barchart.component.js.map