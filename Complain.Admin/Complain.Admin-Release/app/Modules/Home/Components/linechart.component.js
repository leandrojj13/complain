"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var baseapi_service_1 = require("../../../Services/baseapi.service");
var LineChartComponent = (function () {
    /**
     * End Controls
     */
    function LineChartComponent(baseApi) {
        this.baseApi = baseApi;
        this.actualDate = new Date();
        this.months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        /**
         * LineChart
         */
        this.lineChartLabels = this.months;
        this.lineChartData = [
            { data: new Array(this.lineChartLabels.length) },
        ];
        this.lineChartOptions = {
            responsive: true,
            scales: {
                yAxes: [{
                        ticks: {
                            min: 0,
                            suggestedMax: 20
                        }
                    }]
            }
        };
        this.lineChartColors = [
            {
                label: " Denuncias",
                fill: true,
                lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderWidth: 2,
                borderColor: "#18bc9c",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 5,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(24, 188, 156, 0.46)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                spanGaps: false,
            }
        ];
        this.lineChartLegend = true;
        this.lineChartType = 'line';
        /**
         * End LineChart
         */
        /**
         * Controls
         */
        this.disabledYear = false;
        this.years = [];
        this.selectedYear = this.actualDate.getFullYear();
        this.scales = ["Meses", "Años"];
        this.selectedScale = "Meses";
    }
    LineChartComponent.prototype.chartClicked = function (e) {
        console.log(e);
    };
    LineChartComponent.prototype.chartHovered = function (e) {
        console.log(e);
    };
    LineChartComponent.prototype.defaultFuncCallBack = function (response) {
        var newLineChartData = new Array({
            data: new Array(this.months.length)
        });
        response.forEach(function (element) {
            newLineChartData[0].data[element.monthNumber - 1] = element.count;
        });
        this.lineChartData = newLineChartData;
    };
    LineChartComponent.prototype.selectedYearChange = function (value) {
        var _this = this;
        if (value) {
            this.getStatistic(value, function (response) {
                _this.defaultFuncCallBack(response);
            });
        }
    };
    LineChartComponent.prototype.scaleChange = function (value) {
        var _this = this;
        if (value === "Años") {
            this.baseApi.getAll("Complaint/GetComplaintsMadeLineChartAnual", function (response) {
                if (response.length < 3)
                    _this.setYearsAfterAndBefore(response, 2);
                else
                    _this.setYearsAfterAndBefore(response, 2, false);
                var labels = response.map(function (x) { return x.year; });
                var newLineChartData = new Array({
                    data: new Array()
                });
                response.forEach(function (element) {
                    newLineChartData[0].data.push(element.count);
                });
                _this.lineChartData = newLineChartData;
                _this.lineChartLabels = labels;
            });
            this.disabledYear = true;
        }
        else {
            this.getStatistic(this.selectedYear, function (response) {
                _this.defaultFuncCallBack(response);
                _this.lineChartLabels = _this.months;
            });
            this.disabledYear = false;
        }
    };
    LineChartComponent.prototype.setYearsAfterAndBefore = function (response, length, applyBefore) {
        if (applyBefore === void 0) { applyBefore = true; }
        var array = response;
        for (var index = 0; index < length; index++) {
            var lastobj = response[response.length - 1];
            response.push({ year: lastobj.year + 1, count: undefined });
            if (applyBefore) {
                var firstobj = response[0];
                response.unshift({ year: firstobj.year - 1, count: undefined });
            }
        }
    };
    LineChartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getStatistic(this.actualDate.getFullYear(), function (response) {
            _this.defaultFuncCallBack(response);
        });
        this.baseApi.getAll("Complaint/GetYearsWhereComplaintsWereMade", function (response) {
            if (response.indexOf(_this.actualDate.getFullYear()) == -1)
                response.unshift(_this.actualDate.getFullYear());
            _this.years = response;
        });
    };
    LineChartComponent.prototype.getStatistic = function (year, callback) {
        this.baseApi.getAll("Complaint/GetComplaintsMadeLineChartMonthlyByYear?year=" + year, callback);
    };
    return LineChartComponent;
}());
LineChartComponent = __decorate([
    core_1.Component({
        selector: 'linechart-statistics',
        templateUrl: "../../Templates/linechart.template.html"
    }),
    __metadata("design:paramtypes", [baseapi_service_1.BaseapiService])
], LineChartComponent);
exports.LineChartComponent = LineChartComponent;
//# sourceMappingURL=linechart.component.js.map