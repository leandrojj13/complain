"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var home_component_1 = require("./Components/home.component");
var barchart_component_1 = require("./Components/barchart.component");
var linechart_component_1 = require("./Components/linechart.component");
var chartDirective_1 = require("../../Directives/chart/chartDirective");
var mydatepicker_1 = require("mydatepicker");
var HomeModule = (function () {
    function HomeModule() {
    }
    return HomeModule;
}());
HomeModule = __decorate([
    core_1.NgModule({
        declarations: [home_component_1.HomeComponent, barchart_component_1.BarChartComponent, linechart_component_1.LineChartComponent],
        imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, chartDirective_1.ChartsModule, mydatepicker_1.MyDatePickerModule],
        exports: []
    })
], HomeModule);
exports.HomeModule = HomeModule;
//# sourceMappingURL=home.modules.js.map