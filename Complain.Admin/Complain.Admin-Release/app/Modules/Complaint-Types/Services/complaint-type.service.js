"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
require("rxjs/add/operator/toPromise");
var http_1 = require("@angular/http");
var environment_1 = require("../../../Environments/environment");
var ComplaintTypeService = (function () {
    function ComplaintTypeService(http) {
        this.http = http;
        this.apiUrl = environment_1.environment.apiUrl + "complaintType";
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.options = new http_1.RequestOptions({ headers: this.headers });
    }
    ;
    ComplaintTypeService.prototype.getAll = function (callback, errorcalback) {
        this.http.get(this.apiUrl, this.options)
            .toPromise()
            .then(function (httpResult) {
            if (httpResult.ok) {
                var response = httpResult.json();
                if (response.success) {
                    var model = httpResult.json().entities;
                    callback(model, response, httpResult);
                }
                else {
                    if (errorcalback) {
                        errorcalback(response, httpResult);
                    }
                }
            }
            else {
                if (errorcalback) {
                    errorcalback(httpResult);
                }
            }
        })
            .catch(function (error) {
            console.log('An error occurred');
            errorcalback(error);
        });
    };
    ComplaintTypeService.prototype.getBy = function (id, callback, errorcalback) {
        var url = this.apiUrl + "/" + id;
        this.http.get(url, this.options)
            .toPromise()
            .then(function (httpResult) {
            if (httpResult.ok) {
                var response = httpResult.json();
                if (response.success) {
                    var model = httpResult.json().entity;
                    callback(model, response, httpResult);
                }
                else {
                    if (errorcalback) {
                        errorcalback(response, httpResult);
                    }
                }
            }
            else {
                if (errorcalback) {
                    errorcalback(httpResult);
                }
            }
        })
            .catch(function (error) {
            console.log('An error occurred');
            errorcalback(error);
        });
    };
    ComplaintTypeService.prototype.create = function (model, callback, errorcalback) {
        //const options = new RequestOptions({ headers: new Headers({ 'Content-Type': 'multipart/form-data' }) });
        var data = new FormData();
        data.append("model", JSON.stringify(model));
        data.append("image", model.image);
        this.http.post(this.apiUrl + "/Create", data)
            .toPromise()
            .then(function (httpResult) {
            if (httpResult.ok) {
                var response = httpResult.json();
                if (response.success) {
                    var model = httpResult.json().entity;
                    callback(model, response, httpResult);
                }
                else {
                    if (errorcalback) {
                        errorcalback(response, httpResult);
                    }
                }
            }
            else {
                if (errorcalback) {
                    errorcalback(httpResult);
                }
            }
        })
            .catch(function (error) {
            console.log('An error occurred');
            errorcalback(error);
        });
    };
    ComplaintTypeService.prototype.update = function (model, callback, errorcalback) {
        var url = this.apiUrl + "/Update/" + model.id;
        //const options = new RequestOptions({ headers: new Headers({ 'Content-Type': 'multipart/form-data' }) });
        var data = new FormData();
        data.append("model", JSON.stringify(model));
        data.append("image", model.image);
        this.http.put(url, data)
            .toPromise()
            .then(function (httpResult) {
            if (httpResult.ok) {
                var response = httpResult.json();
                if (response.success) {
                    var model = httpResult.json().entity;
                    callback(model, response, httpResult);
                }
                else {
                    if (errorcalback) {
                        errorcalback(response, httpResult);
                    }
                }
            }
            else {
                if (errorcalback) {
                    errorcalback(httpResult);
                }
            }
        })
            .catch(function (error) {
            console.error('An error occurred', error);
            errorcalback(error);
        });
    };
    ComplaintTypeService.prototype.delete = function (id, callback, errorcalback) {
        var url = this.apiUrl + "/" + id;
        this.http.delete(url, this.options)
            .toPromise()
            .then(function (httpResult) {
            if (httpResult.ok) {
                var response = httpResult.json();
                if (response.success) {
                    var model = httpResult.json().entity;
                    callback(model, response, httpResult);
                }
                else {
                    if (errorcalback) {
                        errorcalback(response, httpResult);
                    }
                }
            }
            else {
                if (errorcalback) {
                    errorcalback(httpResult);
                }
            }
        })
            .catch(function (error) {
            console.error('An error occurred', error);
            errorcalback(error);
        });
    };
    return ComplaintTypeService;
}());
ComplaintTypeService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], ComplaintTypeService);
exports.ComplaintTypeService = ComplaintTypeService;
//# sourceMappingURL=complaint-type.service.js.map