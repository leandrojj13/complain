"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var ComplaintTypeModel_1 = require("../../../Models/ComplaintTypeModel");
var complaint_type_service_1 = require("../Services/complaint-type.service");
var $ = require("jquery");
var toastr = require("toastr");
var ComplaintTypeComponent = (function () {
    function ComplaintTypeComponent(complaintTypeService) {
        this.complaintTypeService = complaintTypeService;
        var self = this;
        self.previewImage = "../../content/camera.ico";
        self.complaintTypes = new Array();
        self.model = new ComplaintTypeModel_1.ComplaintTypeModel();
        self.entityToDelete = undefined;
        self.model.imageUrl = self.previewImage;
        self.isLoading = false;
        self.fileReader = new FileReader();
        self.fileReader.onload = function (event) {
            self.model.imageUrl = event.target.result;
        };
        self.getAll = function () {
            self.isLoading = true;
            var callback = function (models, response, httpResult) {
                self.complaintTypes = models;
                self.isLoading = false;
            };
            var errorcallback = function (httpResult, response) {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            };
            self.complaintTypeService.getAll(callback, errorcallback);
        };
        self.save = function () {
            self.isLoading = true;
            var callback = function (model, response, httpResult) {
                toastr["success"]("Registro guardado", "Perfecto");
                self.isLoading = false;
                self.getAll();
                $('.modal').modal("hide");
            };
            var errorcallback = function (response, httpResult) {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Hemos tenido un problema al procesar la petición", "Algo salio mal!");
            };
            if (self.model.id) {
                self.complaintTypeService.update(self.model, callback, errorcallback);
            }
            else {
                self.complaintTypeService.create(self.model, callback, errorcallback);
            }
        };
        self.remove = function (id) {
            self.isLoading = true;
            var callback = function (model, response, httpResult) {
                toastr["success"]("Registro eliminado", "Perfecto");
                $("#ComplaintTypeModalDelete").modal("hide");
                self.getAll();
                self.isLoading = false;
            };
            var errorcallback = function (response, httpResult) {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Hemos tenido un problema al procesar la petición", "Algo salio mal!");
            };
            self.complaintTypeService.delete(id, callback, errorcallback);
        };
        self.showModalToDelete = function (id) {
            self.isLoading = true;
            var callback = function (model, response, httpResult) {
                self.entityToDelete = model;
                self.isLoading = false;
            };
            var errorcallback = function (httpResult, response) {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            };
            self.complaintTypeService.getBy(id, callback, errorcallback);
        };
        self.showModalToEdit = function (id) {
            self.isLoading = true;
            var callback = function (model, response, httpResult) {
                self.model = model;
                self.isLoading = false;
            };
            var errorcallback = function (httpResult, response) {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            };
            self.complaintTypeService.getBy(id, callback, errorcallback);
        };
        self.initVm = function () {
            self.model = new ComplaintTypeModel_1.ComplaintTypeModel();
            self.model.imageUrl = self.previewImage;
            self.isLoading = false;
        };
        self.loadImage = function (event) {
            if (event.target.files && event.target.files[0]) {
                self.model.image = event.target.files[0];
                self.fileReader.readAsDataURL(event.target.files[0]);
            }
        };
    }
    ComplaintTypeComponent.prototype.ngOnInit = function () {
        this.getAll();
    };
    ComplaintTypeComponent.prototype.initInputFile = function () {
        $("#inputfileInput").click();
    };
    return ComplaintTypeComponent;
}());
ComplaintTypeComponent = __decorate([
    core_1.Component({
        selector: 'complaint-types',
        templateUrl: "../../Templates/complaint-type.template.html",
        styleUrls: ['../../../../../styles.css']
    }),
    __metadata("design:paramtypes", [complaint_type_service_1.ComplaintTypeService])
], ComplaintTypeComponent);
exports.ComplaintTypeComponent = ComplaintTypeComponent;
//# sourceMappingURL=complaint-type.component.js.map