"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var UserModel_1 = require("../../../Models/UserModel");
var BroadCastModel_1 = require("../../../Models/BroadCastModel");
var broadcast_service_1 = require("../Services/broadcast.service");
var admin_service_1 = require("../Services/admin.service");
var $ = require("jquery");
var toastr = require("toastr");
var AdminComponent = (function () {
    function AdminComponent(broadCastService, adminService) {
        this.broadCastService = broadCastService;
        this.adminService = adminService;
        var self = this;
        self.isLoading = false;
        self.entityToDelete = undefined;
        // Administradores
        self.administradores = new Array();
        self.adminModel = new UserModel_1.UserModel();
        self.saveAdministrador = function () {
        };
        self.getAllAdministrador = function () {
            self.isLoading = true;
            var callback = function (models, response, httpResult) {
                self.administradores = models;
                self.isLoading = false;
            };
            var errorcallback = function (httpResult, response) {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            };
            //self.complaintTypeService.getAll(callback, errorcallback)
        };
        // BroadCast
        self.broadCasts = new Array();
        self.broadCastModel = new BroadCastModel_1.BroadCastModel();
        self.getAllBroadCast = function () {
            self.isLoading = true;
            var callback = function (models, response, httpResult) {
                self.broadCasts = models;
                self.isLoading = false;
            };
            var errorcallback = function (httpResult, response) {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            };
            self.broadCastService.getAll(callback, errorcallback);
        };
        self.saveBroadCast = function () {
            self.isLoading = true;
            var callback = function (model, response, httpResult) {
                toastr["success"]("Registro guardado", "Perfecto");
                self.isLoading = false;
                self.getAllBroadCast();
                $('.modal').modal("hide");
            };
            var errorcallback = function (response, httpResult) {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Hemos tenido un problema al procesar la petición", "Algo salio mal!");
            };
            if (self.broadCastModel.id) {
            }
            else {
                self.broadCastService.create(self.broadCastModel, callback, errorcallback);
            }
        };
        self.removeBroadCast = function (id) {
            self.isLoading = true;
            var callback = function (model, response, httpResult) {
                toastr["success"]("Registro eliminado", "Perfecto");
                $("#BroadCastModalDelete").modal("hide");
                self.getAllBroadCast();
                self.isLoading = false;
            };
            var errorcallback = function (response, httpResult) {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Hemos tenido un problema al procesar la petición", "Algo salio mal!");
            };
            self.broadCastService.delete(id, callback, errorcallback);
        };
        self.showModalToDeleteBroadCast = function (id) {
            self.isLoading = true;
            var callback = function (model, response, httpResult) {
                self.entityToDelete = model;
                self.isLoading = false;
            };
            var errorcallback = function (httpResult, response) {
                self.isLoading = false;
                console.log(response);
                toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
            };
            self.broadCastService.getBy(id, callback, errorcallback);
        };
        self.initVmBroadCast = function () {
            self.broadCastModel = new BroadCastModel_1.BroadCastModel();
            self.isLoading = false;
        };
    }
    AdminComponent.prototype.ngOnInit = function () {
        this.getAllBroadCast();
        this.getAllUsers();
    };
    AdminComponent.prototype.getAllUsers = function () {
        var _this = this;
        this.isLoading = true;
        var callback = function (models, response, httpResult) {
            _this.users = models;
            _this.isLoading = false;
        };
        var errorcallback = function (httpResult, response) {
            _this.isLoading = false;
            console.log(response);
            toastr["error"]("Algo salio mal!", "Hemos tenido un problema al procesar la petición");
        };
        this.adminService.getAllUsers(callback, errorcallback);
    };
    return AdminComponent;
}());
AdminComponent = __decorate([
    core_1.Component({
        selector: 'admin-home',
        templateUrl: "../../Templates/admin.template.html",
        styleUrls: ['../../../../../styles.css']
    }),
    __metadata("design:paramtypes", [broadcast_service_1.BroadCastService,
        admin_service_1.AdminService])
], AdminComponent);
exports.AdminComponent = AdminComponent;
//# sourceMappingURL=admin.component.js.map