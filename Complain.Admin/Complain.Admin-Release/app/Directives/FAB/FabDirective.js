"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var $ = require("jquery");
'fab';
var FabDirective = (function () {
    function FabDirective(elementRef) {
        this.elementRef = elementRef;
        this.div = elementRef.nativeElement;
    }
    FabDirective.prototype.ngOnDestroy = function () {
    };
    FabDirective.prototype.ngOnChanges = function (changes) {
    };
    FabDirective.prototype.ngOnInit = function () {
        this.refresh();
    };
    FabDirective.prototype.refresh = function () {
        var self = this;
        if ($(this.div).kc_fab)
            $(this.div).kc_fab(this.links);
    };
    return FabDirective;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], FabDirective.prototype, "links", void 0);
FabDirective = __decorate([
    core_1.Component({
        selector: 'fab',
        templateUrl: "App/Components/FAB/Fab.html",
        styles: ['App/Components/FAB/kc.fab.css']
    }),
    __metadata("design:paramtypes", [core_1.ElementRef])
], FabDirective);
exports.FabDirective = FabDirective;
var FabModule = (function () {
    function FabModule() {
    }
    return FabModule;
}());
FabModule = __decorate([
    core_1.NgModule({
        declarations: [
            FabDirective
        ],
        exports: [
            FabDirective
        ],
        imports: []
    })
], FabModule);
exports.FabModule = FabModule;
//# sourceMappingURL=FabDirective.js.map