"use strict";
var StatusComplaint;
(function (StatusComplaint) {
    StatusComplaint[StatusComplaint["Send"] = 0] = "Send";
    StatusComplaint[StatusComplaint["Saved"] = 1] = "Saved";
    StatusComplaint[StatusComplaint["InProcess"] = 2] = "InProcess";
    StatusComplaint[StatusComplaint["Attended"] = 3] = "Attended";
})(StatusComplaint = exports.StatusComplaint || (exports.StatusComplaint = {}));
//# sourceMappingURL=StatusComplaint.js.map