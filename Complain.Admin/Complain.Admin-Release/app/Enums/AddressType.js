"use strict";
var AddressType;
(function (AddressType) {
    AddressType[AddressType["GPS"] = 0] = "GPS";
    AddressType[AddressType["Location"] = 1] = "Location";
})(AddressType = exports.AddressType || (exports.AddressType = {}));
//# sourceMappingURL=AddressType.js.map